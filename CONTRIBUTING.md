# Contributing to fenix-engine-cli

✨ Thanks for contributing to **FeniXEngine - FeniXCLI**! ✨

As a contributor, here are the guidelines we would like you to follow:
- [How can I contribute?](#how-can-i-contribute)
- [Using the issue tracker](#using-the-issue-tracker)
- [Submitting a Merge Request](#submitting-a-merge-request)
- [Coding rules](#coding-rules)
- [Commit message guidelines](#commit-message-guidelines)
- [Working with the code](#working-with-the-code)

We also recommend that you read [How to Contribute to Open Source](https://opensource.guide/how-to-contribute).

## How can I contribute?

### Improve documentation

As a **fenix-engine-cli** user, you are the perfect candidate to help us improve our documentation: typo corrections, clarifications, more examples, etc. Take a look at the [documentation issues that need help](https://gitlab.com/FeniXEngineMV/fenix-cli/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=documentation).

Please follow the [Documentation guidelines](#documentation).

### Give feedback on issues

Some issues are created without information requested in the [Bug report guideline](#bug-report). Help make them easier to resolve by adding any relevant information.

Issues with the [design label]n) are meant to discuss the implementation of new features. Participating in the discussion is a good opportunity to get involved and influence the future direction of **fenix-engine-cli**.

### Fix bugs and implement features

Confirmed bugs and ready-to-implement features are marked with the [help wanted label](https://gitlab.com/FeniXEngineMV/fenix-cli/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=help%20wanted). Post a comment on an issue to indicate you would like to work on it and to request help from the [@fenix-engine-cli/maintainers]() and the community.

## Using the issue tracker

The issue tracker is the channel for [bug reports](#bug-report), [features requests](#feature-request) and [submitting merge requests](#submitting-a-merge-request) only.

Before opening an issue or a Merge Request, please use the [Gitlab issue search](https://docs.gitlab.com/ee/user/search/i) to make sure the bug or feature request hasn't been already reported or fixed.

### Bug report

A good bug report shouldn't leave others needing to chase you for more information. Please try to be as detailed as possible in your report and fill the information requested in the [Bug report template](https://gitlab.com/FeniXEngineMV/fenix-cli/issues/new?).

### Feature request

Feature requests are welcome, but take a moment to find out whether your idea fits with the scope and aims of the project. It's up to you to make a strong case to convince the project's developers of the merits of this feature. Please provide as much detail and context as possible and fill the information requested in the [Feature request template](https://gitlab.com/FeniXEngineMV/fenix-cli/issues/new?template=feature-request.md).

## Submitting a Merge Request

Good merge requests, whether patches, improvements, or new features, are a fantastic help. They should remain focused in scope and avoid containing unrelated commits.

**Please ask first** before embarking on any significant merge requests (e.g. implementing features, refactoring code), otherwise you risk spending a lot of time working on something that the project's developers might not want to merge into the project.

If you have never created a merge request before, welcome 🎉 😄. [Here is a great tutorial](https://opensource.guide/how-to-contribute/#opening-a-pull-request) on how to send one :)

Here is a summary of the steps to follow:

1. [Set up the workspace](#set-up-the-workspace)
2. If you cloned a while ago, get the latest changes from upstream and update dependencies:
```bash
$ git checkout master
$ git pull upstream master
$ rm -rf node_modules
$ npm install
```
3. Create a new topic branch (off the main project development branch) to contain your feature, change, or fix:
```bash
$ git checkout -b <topic-branch-name>
```
4. Make your code changes, following the [Coding rules](#coding-rules)
5. Push your topic branch up to your fork:
```bash
$ git push origin <topic-branch-name>
```
6. [Open a Merge Request](https://help.github.com/articles/creating-a-pull-request/#creating-the-pull-request) with a clear title and description.

**Tips**:
- For ambitious tasks, open a Merge Request as soon as possible with the `[WIP]` prefix in the title, in order to get feedback and help from the community.
- [Allow fenix-engine-cli maintainers to make changes to your Merge Request branch](https://help.github.com/articles/allowing-changes-to-a-pull-request-branch-created-from-a-fork). This way, we can rebase it and make some minor changes if necessary. All changes we make will be done in new commit and we'll ask for your approval before merging them.

## Coding rules

### Source code

To ensure consistency and quality throughout the source code, all code modifications must have:
- No [linting](#lint) errors
- A [test](#tests) for every possible case introduced by your code change
- **100%** test coverage
- [Valid commit message(s)](#commit-message-guidelines)
- Documentation for new features
- Updated documentation for modified features

### Documentation

To ensure consistency and quality, all documentation modifications must:
- Refer to brand in [bold](https://help.github.com/articles/basic-writing-and-formatting-syntax/#styling-text) with proper capitalization, i.e. **GitHub**, **fenix-engine-cli**, **npm**
- Prefer [tables](https://help.github.com/articles/organizing-information-with-tables) over [lists](https://help.github.com/articles/basic-writing-and-formatting-syntax/#lists) when listing key values, i.e. List of options with their description
- Use [links](https://help.github.com/articles/basic-writing-and-formatting-syntax/#links) when you are referring to:
  - a **fenix-engine-cli** concept described somewhere else in the documentation, i.e. How to [contribute](CONTRIBUTING.md)
  - a third-party product/brand/service, i.e. Integrate with [GitHub](https://github.com)
  - an external concept or feature, i.e. Create a [GitHub release](https://help.github.com/articles/creating-releases)
  - a package or module, i.e. The [`@fenix-engine-cli/github`](https://gitlab.com/FeniXEngineMV/fenix-cli/github) module
- Use the the [single backtick `code` quoting](https://help.github.com/articles/basic-writing-and-formatting-syntax/#quoting-code) for:
  - commands inside sentences, i.e. the `fenix-engine-cli` command
  - programming language keywords, i.e. `function`, `async`, `String`
  - packages or modules, i.e. The [`@fenix-engine-cli/github`](https://gitlab.com/FeniXEngineMV/fenix-cli/github) module
- Use the the [triple backtick `code` formatting](https://help.github.com/articles/creating-and-highlighting-code-blocks) for:
  - code examples
  - configuration examples
  - sequence of command lines

### Commit message guidelines

#### Atomic commits

If possible, make [atomic commits](https://en.wikipedia.org/wiki/Atomic_commit), which means:
- a commit should contain exactly one self-contained functional change
- a functional change should be contained in exactly one commit
- a commit should not create an inconsistent state (such as test errors, linting errors, partial fix, feature with documentation etc...)

A complex feature can be broken down into multiple commits as long as each one maintains a consistent state and consists of a self-contained change.

#### Git commit message conventions

FeniXEngine git commit message convention is a gentle mix between clear easy to ready messages with a bit of extra spunk and fun by using gitmoji commit standard.

Each commit message consists of a **title**, and a **message**. The header has a special format that includes a **emoji**, **type**, and a **subject**:

```commit
[emoji]<type> <subject>
<BLANK LINE>
<message>
```

Header format:
```
[:emoji:] <type> <subject>
```

The **emoji** is optional but you can consider starting the commit message with an applicable emoji in the list.

For a full list have a look at [Gitmoji](https://gitmoji.carloscuesta.me/) 🎨

| Emoji | Raw            | Is for                                                                                     |
|-------|----------------|-----------------------------------------|
| 🐛   | :bug:           | Fixing a bug
| ⚡   | :zap:           | Improving performance
| ✨   | :sparkles       | Adding a new feature
| 🎨   | :art:           | Improving structure/format of the code
| 🔥    | :fire:          | Removing code or files
| 📝   | :memo:           | Writing documentation
| ✅   | :white_checkmark | Adding tests        
| 🚧   | :construction:   | Work in progress       
| 🔧   | :wrench:         | Updating/Adding configuration files
| 💚   | :green_heart:    | Fixing CI builds

To make commits with emoji's easier try out the [Gitmoji CLI](https://github.com/carloscuesta/gitmoji-cli)

#### Revert
 
If the commit reverts a previous commit, it should begin with `Revert: `, followed by the header of the reverted commit. The message should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

#### Type

The **type** is mandatory in all commit messages and must begin with an uppercase letter:

| Type         | Description                                                                                                 |
|--------------|-------------------------------------------------------------------------------------------------------------|
| **Chore**    | Changes that affect the build system or external dependencies (example scopes: eslint, npm)         |
| **CI**       | Changes to our CI configuration files and scripts |
| **Docs**     | Documentation only changes                                                                                  |
| **Feat**     | A new feature                                                                                              |
| **Fix**      | A bug fix                                                                                                   |
| **Perf**     | A code change that improves performance                                                                     |
| **Refactor** | A code change that neither fixes a bug nor adds a feature                                                   |
| **Style**    | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)      |
| **Test**     | Adding missing tests or correcting existing tests                                                           |

#### Subject

The subject contains succinct description of the change:

* don't capitalize first letter
* no dot (.) at the end
* use the present tense ("Add feature" not "Added feature")
* use the imperative mood ("Move cursor to..." not "Moves cursor to...")
* limit the first line to 72 characters or less

#### Message
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

It may also contain any information about **Breaking Changes** and is also the place to reference GitHub issues that this commit **Closes**.

**Breaking Changes** should start with the word `Breaking Change:` with a space or two newlines. The rest of the commit message is then used for this.

#### Examples

Remember this is a convention but don't take things so seriously, this is why we added the gitmoji standard to our commitm essage convention

```commit
🐛Fix stop graphite breaking when too much pressure applied`
```

```commit
✨Feat add 'graphiteWidth' option`

Fix #42
```

```commit
⚡ Perf remove 🔥 graphiteWidth option

BREAKING CHANGE 💥 The graphiteWidth option has been removed.

The default graphite width of 10mm is always used for performance reasons.
```

## Working with the code

### Lint

All the [fenix-engine-cli](https://gitlab.com/FeniXEngineMV/fenix-cli) repositories use [Standard]https://standardjs.com/).

Before pushing your code changes make sure there are no linting errors with `npm run lint`. These changes will have to pass the repository pipeline before merge is accepted.

**Tips**:
- Most linting errors can be automatically fixed with `npm run lint --fix`.

### Tests

All the [fenix-engine-cli](https://gitlab.com/FeniXEngineMV/fenix-cli) repositories use [AVA](https://github.com/avajs/ava) for writing and running tests.

Before pushing your code changes make sure all **tests pass** and the **coverage is 100%**:

```bash
$ npm run test
```

**Tips:** During development you can:
- run only a subset of test files with `ava <glob>`, for example `ava test/mytestfile.test.js`
- run in watch mode with `ava -w` to automatically run a test file when you modify it
- run only the test you are working on by adding [`.only` to the test definition](https://github.com/avajs/ava#running-specific-tests)
