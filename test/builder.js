import test from 'ava'
import fs from 'fs-extra'
import eol from 'eol'

import { builder } from '../src/builder.js'
import { PluginRollup } from '../src/rollup.js'
import { logger } from '../src/utils/logger.js'

const target = `${global.FIX_DIR}/plugins/`

test.before(t => {
  logger.setOptions({ silent: true })
})

test.after(async t => {
  await global.cleanAllTemp()
})

test('accepts a string target which reflect a config file', async t => {
  // can't test due to being unable to chdir in workers
  t.pass()
})

test('builds a plugin based on a config path', async t => {
  t.plan(1)
  const destination = global.newTempDir()
  await builder({ target: `${target}/fenix-core/`, destination })
  const files = await fs.readdir(`${destination}/mz/js/plugins/`)
  t.deepEqual(files, ['FeniXCore.js'])
})

test('bundles only the specified plugin directory when using --only', async t => {
  const destination = global.newTempDir()
  await builder({ target, destination, only: ['fenix-core'] })
  const files = await fs.readdir(`${destination}/mz/js/plugins/`)
  t.deepEqual(files, ['FeniXCore.js'])
})

test('bundles all plugins from a multi plugin project', async t => {
  const destination = global.newTempDir()

  await builder({ target, destination, exclusions: ['watcher'] })
  const files = await fs.readdir(`${destination}/mz/js/plugins/`)
  t.deepEqual(files, [
    'FeniXCore.js',
    'FeniXPlugin.js',
    'FeniXPro.js',
    'FeniXPro_Free.js'])
})

test('bundles a single plugin from a multi plugin project', async t => {
  const destination = global.newTempDir()
  await builder({ target: `${target}/fenix-core/`, destination })

  const files = await fs.readdir(`${destination}/mz/js/plugins/`)
  t.deepEqual(files, ['FeniXCore.js'])
})

test('output of plugin should contain all modules bundled', async t => {
  const destination = global.newTempDir()
  await builder({ target: `${target}/fenix-core/`, destination })

  const file = await fs.readFile(`${destination}/mz/js/plugins/FeniXCore.js`, 'utf8')
  t.snapshot(eol.lf(file))
})

test('output of conditional compilation for MV', async t => {
  const destination = global.newTempDir()
  const flags = ['mv']
  await builder({ target: `${target}/fenix-core/`, destination, flags, skipIfMissingFlag: false })

  const file = await fs.readFile(`${destination}/mv/js/plugins/FeniXCore.js`, 'utf8')
  t.snapshot(eol.lf(file))
})

test('output of conditional compilation for Free', async t => {
  const destination = global.newTempDir()
  await builder({ target: `${target}/fenix-core/`, destination, skipIfMissingFlag: false })

  const file = await fs.readFile(`${destination}/mv/js/plugins/FeniXCore_Free.js`, 'utf8')
  t.snapshot(eol.lf(file))
})

test('ignores an output when a tag is unused and ignoreIfMissingTag is true', async t => {
  const destination = global.newTempDir()
  const flags = ['mv', 'pro']
  await builder({
    target: `${target}/fenix-core/`,
    destination,
    flags,
    ignoreIfMissingTag: true
  })

  const files = await fs.readdir(`${destination}/`, 'utf8')
  t.true(!files.includes('mv'), 'MV directory should not exist')
  t.true(files.includes('mz'), 'MZ directory should exist')
})

test('accepts a bundler plugin', async t => {
  const destination = global.newTempDir()
  await builder({
    target: `${target}/fenix-core/main.js`,
    bundler: PluginRollup,
    destination
  })

  const mzFiles = await fs.readdir(`${destination}/mz/js/plugins/`, 'utf8')
  t.deepEqual(mzFiles, ['FeniXCore.js'])
})

test('affix merges with affix from config', async t => {
  const destination = global.newTempDir()
  await builder({
    target: `${target}/free-pro-plugin/main.js`,
    bundler: PluginRollup,
    destination,
    affix: 'Beta'
  })

  const mzFiles = await fs.readdir(`${destination}/mz/js/plugins/`, 'utf8')
  t.deepEqual(mzFiles, ['FeniXPro_Beta.js', 'FeniXPro_Free_Beta.js'])
})

test('generates a sourcemap', async t => {
  const destination = global.newTempDir()
  await builder({
    target: `${target}/fenix-core/main.js`,
    bundler: PluginRollup,
    sourcemap: true,
    destination
  })

  const file = await fs.readFile(`${destination}/mz/js/plugins/FeniXCore.js`, 'utf8')
  t.snapshot(eol.lf(file))
})

test('TypeScript - Builds a plugin that uses TypeScript', async t => {
  const destination = global.newTempDir()
  await builder({
    target: `${target}/fenix-core-ts/main.ts`,
    bundler: PluginRollup,
    destination
  })

  const file = await fs.readFile(`${destination}/mz/js/plugins/FeniXCore.js`, 'utf8')
  t.snapshot(eol.lf(file))
})

test('TypeScript - Builds a plugin with conditional compilation for MV', async t => {
  const destination = global.newTempDir()
  await builder({
    target: `${target}/fenix-core-ts/main.ts`,
    bundler: PluginRollup,
    destination,
    skipIfMissingFlag: false
  })

  const file = await fs.readFile(`${destination}/mv/js/plugins/FeniXCore.js`, 'utf8')

  t.is(file.includes('isMv'), true)
  t.snapshot(eol.lf(file))
})
