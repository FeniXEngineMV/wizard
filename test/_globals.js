import tempy from 'tempy'
import fs from 'fs-extra'
import { fileURLToPath } from 'url'
import { dirname } from 'path'

const __filename = fileURLToPath(import.meta.url)
const __dirname = dirname(__filename)
global.ALL_TEMP_DIRS = []
global.TEMP_DIR = `${__dirname}/temp/`
global.FIX_DIR = `${__dirname}/fixtures/`

global.newTempDir = function () {
  const newTempDir = tempy.directory()
  global.ALL_TEMP_DIRS.push(newTempDir)
  return newTempDir
}

global.cleanAllTemp = async function () {
  try {
    if (await fs.exists(global.TEMP_DIR) && !process.env.CI) {
      await fs.emptyDir(global.TEMP_DIR)
    }

    if (global.ALL_TEMP_DIRS.length > 0 && !process.env.CI) {
      for (const dir of global.ALL_TEMP_DIRS) {
        if (await fs.exists(dir)) {
          await fs.remove(dir)
        }
      }
    }
    Promise.resolve()
  } catch (error) {
    throw new Error(error)
  }
}
