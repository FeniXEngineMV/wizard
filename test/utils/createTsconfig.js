import test from 'ava'
import fs from 'fs-extra'

import { createTsconfig } from '../../src/utils/index.js'

test.after(async t => {
  await global.cleanAllTemp()
})

test('creates package.json and writes options', async t => {
  const output = global.newTempDir()
  await createTsconfig({
    destination: output
  })

  t.is(await fs.exists(`${output}/tsconfig.json`), true, 'tsconfig.json should be written to disk')

  const tsconfigData = await fs.readJson(`${output}/tsconfig.json`)
  t.is(typeof tsconfigData, 'object', 'tsconfigData.json should should have a compilerOptions object in it')
  t.is(tsconfigData.include.includes('src/**/*'), true, 'tsconfigData.json should have an include array in it')
})
