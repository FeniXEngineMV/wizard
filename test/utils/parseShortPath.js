import test from 'ava'
import upath from 'upath'

import { parseShortPath } from '../../src/utils/index.js'

test('parses a full path to a relative path from the cwd', async t => {
  const filepath = upath.normalize(`${global.FIX_DIR}/plugins/fenix-core/main.js`)
  const shortPath = upath.normalize(parseShortPath(filepath))
  const expected = upath.normalize('test/fixtures/plugins/fenix-core/main.js')
  t.is(shortPath, expected)
})
