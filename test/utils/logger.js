
import test from 'ava'
import { logger, Logger } from '../../src/utils/index.js'
import { stdout } from 'test-console'

const arrayIncludes = (arr, str) => arr.some(el => el.includes(str))

test('returns an object', async t => {
  t.is(typeof logger === 'object', true, 'logger instance should be an object')
})

test.serial('logs messages, warnings and errors', async t => {
  const logger = Logger()
  const inspect = stdout.inspect()
  await logger.log('a message')
  await logger.warn('a warning')
  logger.error('an error')

  inspect.restore()

  const output = inspect.output
  t.true(output.length > 0)
  t.is(arrayIncludes(output, 'a message'), true)
  t.is(arrayIncludes(output, 'a warning'), true)
  t.is(arrayIncludes(output, 'an error'), true)
})

test.serial('respects silent option and does not log', async t => {
  const logger = Logger()
  logger.setOptions({ silent: true })
  const output = await stdout.inspectAsync(async () => {
    await logger.log('a message')
    await logger.warn('a warning')
    logger.error('an error')
  })
  t.deepEqual(output, [], 'should return empty array(console) as nothing should be logged')
})
