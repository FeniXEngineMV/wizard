import test from 'ava'

import { hasMain } from '../../src/utils/index.js'

const srcDir = `${global.FIX_DIR}/plugins/`
const pluginPath = `${srcDir}/fenix-core/`

test('resolves and throws', async t => {
  const doesHaveMain = await hasMain(pluginPath)
  const noMain = await hasMain(srcDir)
  t.truthy(doesHaveMain, 'should return true when main.js is in directory')

  t.falsy(noMain, 'should return false when no main.js in directory')
})
