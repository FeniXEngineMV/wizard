import test from 'ava'

import { walkDirectory } from '../../src/utils/walkDirectory.js'

const srcDir = `${global.FIX_DIR}/plugins/fenix-core/`

test('walks a directory and returns correct paths', async t => {
  await t.notThrowsAsync(walkDirectory(srcDir))
  await t.throwsAsync(walkDirectory(`${srcDir}/notadir/`))

  const files = await walkDirectory(srcDir)
  t.is(Array.isArray(files), true, 'Should resolve to array')
  t.is(files.length === 4, true, 'Should contain 5 paths')
})
