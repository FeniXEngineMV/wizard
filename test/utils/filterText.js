import test from 'ava'
import { readFile } from 'fs/promises'
import { filterText } from '../../src/utils/index.js'

const pattern = new RegExp(`(@${'pluginname'})([^\\r\\n]*)`, 'g')

test('Captures and returns regular expression matches', async t => {
  t.plan(4)
  const parameters = await readFile(`${global.FIX_DIR}/plugins/fenix-core/Parameters.js`)

  filterText(parameters, pattern, (matches) => {
    t.is(matches.length > 0, true, 'Should contain more than one match')
    t.deepEqual(matches[0], '@pluginname FeniXCore', 'Full match')
    t.deepEqual(matches[1], '@pluginname', 'should be first capture group, the tag')
    t.deepEqual(matches[2].trim(), 'FeniXCore', 'Should be second capture group, value of the tag')
  })
})
