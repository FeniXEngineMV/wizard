import test from 'ava'

import { getConfig } from '../../src/utils/index.js'
import path from 'path'

test('Fetches a config file from a path and returns a resolved config', async t => {
  const config = await getConfig('./test/fixtures/plugins')

  t.is(typeof config, 'object', 'Should be an object')
  t.is(config.target.includes('fenix-core'), true, 'Should contain the correct target')
  t.is(config.output.length, 2, 'Should contain two outputs')
  t.is(path.isAbsolute(config.target), true, 'Target should be an absolute path')

  for (const output of config.output) {
    t.is(path.isAbsolute(output.destination), true, 'Destination should be an absolute path')
  }
})

test('Accepts an object and merges the config (config takes precedence)', async t => {
  // get a temp dir
  const tempDir = global.newTempDir()
  const config = await getConfig({
    config: './test/fixtures/plugins',
    target: './test/fixtures/plugins/fenix-core/',
    flags: ['test'],
    destination: tempDir
  })

  t.is(typeof config, 'object', 'Should be an object')
  t.is(config.target.includes('fenix-core'), true, 'Should contain the correct target')

  const hasFreeTag = config.output.some(output => output.flags.includes('free'))
  t.is(hasFreeTag, true, 'Should contain config flags too')

  const hasTestTag = config.output.some(output => output.flags.includes('test'))
  t.is(hasTestTag, true, 'Should contain the tag passed in')

  for (const output of config.output) {
    t.is(output.destination.includes(tempDir), true, 'Should contain the destination from CLI (it takes precedence)')
  }
})

test('Accepts an object and merges default config with options passed in (options take precedence)', async t => {
  // get a temp dir
  const tempDir = global.newTempDir()
  const config = await getConfig({
    target: './test/fixtures/plugins/testing/',
    flags: ['test'],
    destination: tempDir
  })

  t.is(typeof config, 'object', 'Should be an object')
  t.is(config.target.includes('testing'), true, 'Should contain the correct target')
  const hasFreeTag = config.output.some(output => output.flags.includes('free'))
  t.is(hasFreeTag, true, 'Should contain config flags too')

  const hasTestTag = config.output.some(output => output.flags.includes('test'))
  t.is(hasTestTag, true, 'Should contain the tag passed in')

  for (const output of config.output) {
    t.is(output.destination.includes(tempDir), true, 'Should contain the destination from config (it takes precedence)')
  }
})
