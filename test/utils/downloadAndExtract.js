import test from 'ava'
import fs from 'fs-extra'

import { downloadAndExtract } from '../../src/utils/index.js'

import { logger } from '../../src/utils/logger.js'

test.before(t => {
  logger.setOptions({ silent: true, useSpinner: false })
})

test.after(async t => {
  await global.cleanAllTemp()
})

const MV_GAME_FILENAME = 'fenix-lightweight-mv-game-v0.0.1.zip'
const MV_GAME_LINK = `https://gitlab.com/FeniXEngineMV/fenix-lightweight-mv-game/-/archive/v0.0.1/${MV_GAME_FILENAME}`

test('downloads and extracts without errors', async t => {
  await t.notThrowsAsync(downloadAndExtract(MV_GAME_LINK, global.newTempDir(), MV_GAME_FILENAME, 'mv'))
})

test('downloaded data contains files from extracted zip', async t => {
  const destination = global.newTempDir()
  await downloadAndExtract(MV_GAME_LINK, `${destination}/demo`, MV_GAME_FILENAME, 'mv')
  const directory = await fs.readdir(`${destination}/demo`)
  t.true(directory.includes('mv'), 'should contain mv directory')

  const files = await fs.readdir(`${destination}/demo/mv`)
  t.true(files.includes('Game.rpgproject'), 'mv directory should contain Game.rpgproject')
  t.true(files.includes('audio'), 'mv directory should contain audio directory')
})
