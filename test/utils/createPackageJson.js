import test from 'ava'
import fs from 'fs-extra'

import { createPackageJson } from '../../src/utils/index.js'

test.after(async t => {
  await global.cleanAllTemp()
})

test('creates package.json and writes options', async t => {
  const output = global.newTempDir()
  await createPackageJson({
    destination: output,
    name: 'test',
    author: 'test author',
    description: 'test description'
  })
  t.is(await fs.exists(`${output}/package.json`), true, 'package.json should be written to disk')
  const packageData = await fs.readJson(`${output}/package.json`)
  t.is(typeof packageData, 'object', 'package.json should exists as an object')
  t.is(packageData.name, 'test', 'package.json should write given data from options')
  t.is(packageData.author, 'test author', 'package.json should write given data from options')
  t.is(packageData.description, 'test description', 'package.json should write given data from options')
})
