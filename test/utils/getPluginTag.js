import test from 'ava'

import { getPluginTag, filterText, logger } from '../../src/utils/index.js'
import { readFile } from 'fs/promises'

const srcDir = `${global.FIX_DIR}/plugins/`
const pluginPath = `${srcDir}/fenix-core/Parameters.js`

test.before(t => {
  logger.setOptions({ silent: true })
})

test('resolves', async t => {
  const parameters = await readFile(pluginPath)
  await t.notThrowsAsync(getPluginTag(parameters, 'plugindesc'))
})

test('regular expression returns correct matches', async t => {
  const pattern = new RegExp(`(@${'pluginname'})([^\\r\\n]*)`, 'g')
  filterText(await readFile(pluginPath), pattern, (matches) => {
    t.deepEqual(matches[0], '@pluginname FeniXCore')
    t.deepEqual(matches[2], ' FeniXCore')
  })
})

test('finds tag and returns value', async t => {
  const parameters = await readFile(pluginPath)
  const name = await getPluginTag(parameters, 'pluginname')
  const module = await getPluginTag(parameters, 'modulename')
  const author = await getPluginTag(parameters, 'author')
  t.deepEqual(name, 'FeniXCore')
  t.deepEqual(author, 'FeniXEngine Contributors')
  t.deepEqual(module, 'FeniX')
})

test('returns calls onWarn when tag not found', t => {
  t.plan(1)

  getPluginTag(pluginPath, 'external', (message) => {
    t.is(message, 'Tag @external not found in Parameters.js')
  })
})
