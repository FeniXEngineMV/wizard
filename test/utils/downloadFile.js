import test from 'ava'

import { downloadFile } from '../../src/utils/index.js'

import { logger } from '../../src/utils/logger.js'

test.before(t => {
  logger.setOptions({ silent: true })
})

test.after(async t => {
  await global.cleanAllTemp()
})
const DOWNLOAD_LINK = `https://gitlab.com/FeniXEngineMV/plugins/-/raw/v1-release/releases/LTN_ItemCategories.js`

test('downloads and resolves with good link', async t => {
  await t.notThrowsAsync(downloadFile({ url: DOWNLOAD_LINK }))
})

test('downloaded data is a buffer', async t => {
  const downloadedData = await downloadFile({ url: DOWNLOAD_LINK })
  t.true(Buffer.isBuffer(downloadedData), 'Data should be a buffer')
})

// test('rejects when url is bad', async t => {
//   const error = await t.throwsAsync(downloadFile({ url: 'https://httpstat.us/500' }))
//   t.is(error.message, 'Failed to load, response status code is 500')
// })
