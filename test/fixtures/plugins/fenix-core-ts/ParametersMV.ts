/*:
 * @pluginname FeniXCore
 * @plugindesc (MV) The core plugin for FeniXEngine which provides a robust API for plugin developers.
 *
 * @author FeniX Contributors (https://fenixenginemv.gitlab.io/)
 *
 * @modulename FeniX
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 The plugin may be used in commercial and non-commercial products.
 For full license details visit https://fenixenginemv.gitlab.io//License

 Please report all bugs to https://fenixenginemv.gitlab.io//Support

 --------------------------------------------------------------------------------
 # INFORMATION
 FeniXEngine plugin for use with RPG Maker MV
*/
