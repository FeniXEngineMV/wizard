/**
 * The plugins Core file, which contains registration and export of important
 * members.
 *
 * @file Core
 *
 * @author       FeniXEngine Contributers
 * @copyright    2018 FeniX Engine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/develop/LICENSE|MIT License}
 */

/**
 * The version of this plugin
 * @memberof FeniX
*/
export const VERSION = '2.0.0'

// #if _MV
export const isMv = true
// #elif _MZ
export const isMz = true
// #endif
