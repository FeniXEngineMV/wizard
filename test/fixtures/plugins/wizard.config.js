// technically wizard.config.js should be at root of the project
// but since the root of the project is not a plugin we improvise for a mock config
export default
{
  target: './fenix-core',
  bundler: 'default',
  output: [
    {
      flags: ['free', 'mv'],
      destination: 'games/',
      gameDir: 'mv',
      parameters: 'ParametersMV.js'
    },
    {
      flags: ['free', 'mz'],
      destination: 'games/',
      gameDir: 'mz',
      parameters: 'Parameters.js'
    }
  ]
}
