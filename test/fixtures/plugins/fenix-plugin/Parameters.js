/**
 * All plugin parameters for this plugin
 *
 * @file parameters
 *
 * @author       FeniXEngine Contributors
 * @copyright    2018 FeniXEngine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/release/LICENSE|MIT License}
 */

/*:
 * @pluginname FeniXPlugin
 * @target MZ
 * @plugindesc The core plugin for FeniXEngine which provides a robust API for plugin developers.
 * @url https://fenixenginemv.gitlab.io/
 *
 * @author FeniX Contributor
 *
 * @modulename
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 The plugin may be used in commercial and non-commercial products.
 For full license details visit https://fenixenginemv.gitlab.io//License

 Please report all bugs to https://fenixenginemv.gitlab.io//Support

 --------------------------------------------------------------------------------
 # INFORMATION

 FeniX Engine core plugin is required for all other FNX plugins to work.
This core plugin contains common and useful functions for easier plugin development.

*/
