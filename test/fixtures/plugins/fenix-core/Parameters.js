/**
 * All plugin parameters for this FeniXEngine MV plugin
 *
 * @file parameters
 *
 * @author       FeniXEngine Contributors
 * @copyright    2018 FeniXEngine
 * @license      {@link https://gitlab.com/FeniXEngineMV/plugins/blob/release/LICENSE|MIT License}
 */

/*:
 * @pluginname FeniXCore
 * @plugindesc The core plugin for FeniXEngine which provides a robust API for plugin developers.
 * @target mz
 * @author FeniX Contributors
 * @url https://fenixenginemv.gitlab.io/
 *
 * @modulename FeniX
 *
 * @help
--------------------------------------------------------------------------------
 # TERMS OF USE

 The plugin may be used in commercial and non-commercial products.
 For full license details visit https://fenixenginemv.gitlab.io//License

 Please report all bugs to https://fenixenginemv.gitlab.io//Support

 --------------------------------------------------------------------------------
 # INFORMATION
 FeniXEngine plugin for use with RPG Maker MV
*/
