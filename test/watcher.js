import test from 'ava'
import fs from 'fs'

import { createFileWatcher, watch } from '../src/watcher.js'
import { logger } from '../src/utils/logger.js'
import { stdout } from 'test-console'

const fsp = fs.promises
const TARGET_DIR = `${global.FIX_DIR}/watcher/`

const arrayIncludes = (arr, str) => arr.some(el => el.includes(str))
const ORIGINAL_DATA = {
  core: null,
  empty: null
}

function timeout (ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

test.before(async t => {
  logger.setOptions({ silent: true })
  ORIGINAL_DATA.core = await fsp.readFile(`${TARGET_DIR}/fenix-core/main.js`)
  ORIGINAL_DATA.empty = await fsp.readFile(`${TARGET_DIR}/fenix-plugin/main.js`)
})

test.after.always(async t => {
  await global.cleanAllTemp()
  await fsp.writeFile(`${TARGET_DIR}/fenix-core/main.js`, ORIGINAL_DATA.core)
  await fsp.writeFile(`${TARGET_DIR}/fenix-plugin/main.js`, ORIGINAL_DATA.empty)
})

test('fileChangeWatcher is a function', async t => {
  t.is(typeof createFileWatcher, 'function', 'should return as function')
})

// test('fileChangeWatcher logs message on ready', async t => {
//   logger.setOptions({ silent: false })
//   const inspect = stdout.inspect()
//   createFileWatcher(`${TARGET_DIR}/fenix-core/`)
//   await timeout(5000)
//   t.is(arrayIncludes(inspect.output, 'Waiting for changes...'), true)
//   inspect.restore()
//   logger.setOptions({ silent: true })
// })

// test('watch is a function', t => {
//   t.is(typeof watch, 'function', 'should return as function')
// })

// test.only('watch triggers listeners on change', async t => {
//   t.plan(1)

//   const path = `${TARGET_DIR}/fenix-plugin/`

//   watch(path, async (path, stats) => {
//     t.pass('File changed')
//     listenerCalled = true
//   })
//   await timeout(2000)
//   let listenerCalled = false

//   // watcher.addListener(async (path, stats) => {
//   //   t.pass('File changed')
//   //   listenerCalled = true
//   // })

//   await fsp.writeFile(`${path}/main.js`, '//testing bundle')

//   await timeout(2000)
//   t.is(listenerCalled, true, 'Listener should be called')
// })
