import test from 'ava'
// import http from 'http'
// import fetch from 'node-fetch'
// import eol from 'eol'
import fs from 'fs'
// import WebSocket from 'ws'

import { startServer } from '../src/server.js'
import { logger } from '../src/utils/logger.js'

// const TARGET_DIR = `${global.FIX_DIR}/plugins/`
const SERVER_DIR = global.newTempDir()
const fsp = fs.promises

test.before(async t => {
  logger.setOptions({ silent: true })
  const indexContents = await fsp.readFile(`${global.FIX_DIR}/index.html`)
  await fsp.writeFile(`${SERVER_DIR}/index.html`, indexContents)
})

test.after(async t => {
  await global.cleanAllTemp()
})

test.serial('startServer is a function', t => {
  t.is(typeof startServer, 'function', 'should return as function')
})

// test.serial('startServer returns object with socketServer and app', t => {
//   const server = startServer({
//     target: `${TARGET_DIR}/fenix-core/`,
//     destination: SERVER_DIR,
//     port: 1919
//   })

//   t.is(
//     server.app instanceof http.Server,
//     true,
//     'should return as instance of http.Server'
//   )
//   t.is(
//     server.socketServer instanceof WebSocket.Server,
//     true,
//     'should return as instance of WebSocket.Server'
//   )
//   server.app.close()
//   server.socketServer.close()
// })

// test.serial('startServer opens port 1818 by default', async t => {
//   t.plan(1)
//   const server = startServer({
//     target: `${TARGET_DIR}/fenix-core/`,
//     destination: SERVER_DIR
//   })

//   const response = await fetch('http://localhost:1818/index.html')
//   const text = await response.text()
//   t.snapshot(eol.lf(text))
//   server.app.close()
//   server.socketServer.close()
// })

// test.serial('startServer injects reload client', async t => {
//   t.plan(1)
//   const server = startServer({
//     target: `${TARGET_DIR}/fenix-core/`,
//     destination: SERVER_DIR
//   })

//   const response = await fetch('http://localhost:1818/reload-client.js')
//   const text = await response.text()
//   t.snapshot(eol.lf(text))
//   server.app.close()
//   server.socketServer.close()
// })
