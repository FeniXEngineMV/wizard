const config = require('./docs/_data/config')
const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const pluginTOC = require('./docs/plugins/plugin-toc/.eleventy.js')

const { EleventyHtmlBasePlugin } = require('@11ty/eleventy')

module.exports = function (eleventyConfig) {
  const isProduction = process.env.NODE_ENV === 'production'

  eleventyConfig.addPassthroughCopy('docs/css')
  eleventyConfig.addPassthroughCopy('docs/img')
  const mdOptions = {
    html: true
  }
  eleventyConfig.setLibrary('md', markdownIt(mdOptions).use(markdownItAnchor))
  eleventyConfig.addPlugin(pluginTOC, {
    ul: true,
    wrapper: 'aside',
    wrapperClass: 'menu',
    tags: ['h1', 'h2', 'h3', 'h4'],
    customToc: config.customToc
  })

  eleventyConfig.addPlugin(EleventyHtmlBasePlugin, {
    baseHref: isProduction ? '/wizard/' : '/',
    extensions: 'html'
  })

  eleventyConfig.addPairedShortcode('message', (content, label = 'Note', type = 'is-dark') => {
    return `
      <article class="message ${type}">
        <div class="message-header">
          <p>${label}</p>
        </div>
        <div class="message-body">
          ${content}
        </div>
      </article>
    `
  })

  eleventyConfig.addPairedShortcode('message', (content, label = 'Note', type = 'is-dark') => {
    return `
      <article class="message ${type}">
        <div class="message-header">
          <p>${label}</p>
        </div>
        <div class="message-body">
          ${content}
        </div>
      </article>
    `
  })

  return {
    dir: {
      input: 'docs',
      output: 'public'
    }
  }
}
