---
layout: /layouts/documentation.html
---

# CLI Options

## Init Command
The init command is a guisidebar: autoded project setup to get you started developing plugins fast. It will ask you which packages you would like to use in your project and then will proceed to installing them.

```bash
fenix

# or

fenix init
```

### Screenshot
![fenix-cli-project-setup](/img/fenixwizard-init.gif)

### Project Directory
* Prompt: Where would you like to create your project

* Type: `String`

* Default: `./`

> This is the location `FeniXWizard` will create the project structure, install packages and download the game demo.

### Project Name
* Prompt: Project Name

* Type: `String`
`
* Default: `my-generated-project`

> The name of the project, this is used in the project's `package.json`.

### Project Description
* Prompt: Project Description

* Type: `String`

* Default: ``

> The description of the project, this is used in the project's `package.json`.

### Project Author
* Prompt: Project Author

* Type: `String`

* Default: `FeniXEngine Contributors`

> The authors name for the project, this is used in the project's `package.json`.

### Linter
* Prompt: Choose a linter

* Type: `Choice`

* Default: `ESLint`

* Values:

  * [ESLint](https://www.npmjs.com/package/eslint)

  * [XO](https://www.npmjs.com/package/xo)

  * [JSHint](https://www.npmjs.com/package/jshint)

> The linter to use to lint for errors in your plugins. We reccomend ESLint for it's large rule set and shareable configs.

### Linter Plugins
Prompt: Which ESLint plugins would you like to install

* Type: `Array | Multiple Choice`

* When: ESLint is chosen as your linter

* Default: `[]`

* Values:

  * [RPG Maker Global Variables Plugin](https://www.npmjs.com/package/eslint-plugin-rpgmaker)

  * [Standard ESLint Config](https://www.npmjs.com/package/eslint-config-standard)

  * [Standard /w Semicolons ESlint Config](https://www.npmjs.com/package/eslint-config-semistandard)

  * [Airbnb ESLint Config](https://www.npmjs.com/package/eslint-config-airbnb)

> Important and helpful ESLint plugins and configurations, these are automatically configured during setup.


### Other Packages
* Prompt: Would you like to install other useful packages

* Type: `Array | Multiple Choice`

* Default: `[]`

* Values:

  * [FeniXTools - A modular RPG Maker plugin Library](https://www.npmjs.com/package/fenix-tools)

  * [Ava - Futuristic test runner](https://www.npmjs.com/package/ava)

  * [Flow - Static type checker for JavaScript](https://www.npmjs.com/package/flow)

> Optional but useful packages which will help develop plugins.

### Initialize Git Repository
* Prompt: Would you like to initialize git for this project?

* Type: `Boolean | Yes/No`

* Default: `false`

> Initializes a git repository in the destination directory

### Download Demo
* Prompt: Download FeniX Lightweight MV Demo game

* Type: `Boolean | Yes/No`

* Default: `false`

> Downloads a lightweight RPG Maker game demo __(6.7mb)__, and extracts it to the proejcts `game/` directory.



## Serve Command
The serve command will start a server at a default address of `localhost:1818` and serve all files from the default `'./game/` and listen for changes in the `./src` directory. If changes are made then it will continue to re-bundle the plugin(s) to the './game/js/plugins` directory and reload any browser opened at the default address.

 ```bash
 fenix server
 ```

### Server Destination

 * arg: `-d <path>`, `--destination <path>`
 * type: `String`
 * default `./game`

 > The location to serve files from, usually a game project location where all plugins are built to.

### Server target

 * arg: `-t <path>`, `--target <path>`
 * type: `String`
 * default `./src`

 > The location of plugin source files to build and watch for changes. All files are built to the server's destination directory in js/plugins

### Port

 * arg: `-p`, `--port`
 * type: `Number`
 * default `1818`

 > Changes the port the server listens on. Useful when other applications are using the same port.

## Build Command
The build command will bundle your plugins to a location you choose. By default the build command will bundle all plugins it can find in the `./src` directory and write them to `./game/js/plugins/`. The build command also has the option of watching for changes without serving the game.

 ```bash
 fenix build
 ```

### Build Target

 * arg: `-t <path>`, `--target <path>`
 * type: `String`
 * default `./src/`

 > the target directory where your plugins are located. The location of the plugins or a directory containing the `main.js` and `parameters.js` files.

 ### Destination

 * arg: `-d <path>`, `--destination <path>`
 * type: `String`
 * default `./game/js/plugins/`

 > the path to save file after the bundle process is finished.

 ### Watch For Changes

 * arg: `-w <path>`, `--watch <path>`
 * type: `String`
 * default `./src/`

 > watch the source files for changes and re-bundle plugin(s)

 ### Generate Sourcemaps

 * arg: `-s`, `--sourcemap`
 * type: `String`

 > Generates sourcemap files for your bundles plugin

 ### Custom Tags

 * arg: `-st`, `--tags`
 * type: `String`

 > Add custom tags to the build for use in your source code. Tags must use commas as spaces for example `fenix build -t tag1,tag2,tag3


 ### Add Affix to Filename

 * arg: `-a`, `--affix <string>`
 * type: `String`

 > Add an affix to the filename for each build.

 ### Only Build Specific Directory

 * arg: `-o`, `--only <directoryName>`
 * type: `String`

 > If you have multiple plugins in the source directory, then you can choose to only build one plugin based on the directory name.

### Skip Missing tags

 * arg: `-m`, `--skipMissingTag`
 * type: `String`

 > This will override the `skipIfMissingTag` settings in the config. [See Auto Skip Builds if Missing Tags](/guide/conditional-compilation/#auto-skip-builds-if-missing-tags)
