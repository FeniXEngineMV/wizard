---
layout: /layouts/documentation.html
---

# Bundler Plugin System

FeniXWizard now lets you create and use plugins. Instead of being forced to use
the default RollupJS bundler, you can now create or use a plugin which will use your preferred bundler, like Webpack!

## How to Use a Plugin
Using a plugin requires the use of the Node API, this is done by importing FeniXWizard.

```js
import wizard from '@fenixengine/wizard'
import webpack from '@fenixengine/wizard-plugin-webpack'
// Note, the webpack plugin does not actually exist, this is for demonstration purposes only

async function build () {
  const options = {
    bundler: webpack
  }
  await wizard.builder(options)
}
```

## How to Create Your Own Plugin
All plugins are simply a function which returns an object, this object must contain an async `bundle` function.
The bundle function will have [options](#options) which you can use to ensure the plugin correctly builds the RPG Maker plugin with the
correct information.

```js
export default function () {
  return {
    async bundle (options) {
      try {
          // Options from `FeniXWizard` will be available here
         /**
          * target - The target directory, where the plugin files are contained
          * destination - The destination directory where the plugin will be written too
          * filename - The filename of the plugin
          * and more......
          *
          */
      } catch (error) {
        throw error
      }
    }
  }
}
```

### Working Example Plugin
For a working example plugin, have a look at the integrated [Rollup plugin](https://gitlab.com/FeniXEngineMV/fenix-cli/blob/master/src/rollup.js) `FeniXWizard` uses.

## Conventions
* Plugins should be prefixed with wizard-plugin-[plugin-name]
* Include wizard-plugin as a keyword in your package
* Use async when possible
* Document your plugin in English

## Options
Because `FeniXWizard` was created with RollupJS as the core bundler, many of the options have the same naming scheme as RollupJS's options object.

`target` - The target directory, where the plugin files are contained. Your plugin should create the final bundle with the files located at this location.

`destination` - The destination directory where the plugin will be written too. Your plugin's final bundle should output to this location.

`filename` - The filename of the plugin. Your plugin should output the file as the name given here.

`modulename` - If using an iife, this will be the variable name for exports. Your plugin should be able to create an iife or create a variable which contains all the RPG Maker plugin's exports.

`parameters` - This will contain the plugin's parameters.js contents. Your plugin should append the parameters to the top of the final bundle.

`logger` - This is `FeniXWizard`'s logger, which can be used to log errors & warnings. If your plugin receives errors or warnings, then you should properly handle them with `FeniXWizard`'s built in logger.

`sourcemap` - Boolean, returns true if sourcemaps should be generated. Your plugin should help create sourcemaps for the final bundle so debugging is possible and easily accomplished.

`footer` - Contents for the footer of the file.

`external` - external modules that the bundler should ignore. External modules are modules which you don't require to be included in the final bundle.
