---
layout: /layouts/documentation.html
---

# Introduction

`FeniXWizard` is a lightweight CLI for use with FeniXEngine plugins and tools. `FeniXWizard` will help you get started with developing plugins for RPG Maker, it does this by combining necessary development tools. These tools help with the bundling of all plugins and serving files to a server for easy testing and streamlined productivity.

## How It Works
`FeniXWizard` is a pre-configured Next-Gen JavaScript module bundler, that searches a projects directory for a `main.js` file(s) located within your `./src/` directory of your project. And for each `main.js` `FeniXWizard` finds, it will build a plugin based on the information in the plugins parameters and contents of the `main.js`.

`FeniXWizard` is powered by [rollup.js](https://rollupjs.org/guide/en)
> Rollup is a module bundler for JavaScript which compiles small pieces of code into something larger and more complex, such as a library or application.

`FeniXWizard` takes all the configuration and integrates it to an RPG Maker plugin development environment. It does this by using the RPG Maker plugin [parameters](parameters.html) as a location for minor configuration and it relays this to rollup.js so you don't have to configure it.

## Features

* Auto ESLint Configuration
* TypeScript Support <Badge text="new" type="warn"/> <Badge text="3.0.0+"/>
* Next-Gen JavaScript Bundler
* Auto Initialize a New Plugin Project
* Conditional Compilation
* Listen to Changes
* Serve an RPG Maker Game Project
  * Live reload

## Why

### Modules- Next-Gen JavaScript
Now that RPG Maker 1.6.1 and modern browsers comes packed with Next-Gen JavaScript, it's a good time to start using it to develop your plugins. The most useful aspect bis being able to use the module syntax and the keywords `import` and `export`. This will help by dividing your plugin into multiple files instead of one large plugin. Doing this lets you contain your logic and work with less code at a time. This help keep your project organized and easily managed by you or a team.

### No configuration

`FeniXWizard`'s mission is to make it so you never have to worry about doing anything other than developing your plugins. Being able to setup a project and start programming RPG Maker plugins instantly is good for those new to plugin development and efficient for the more familiar developers. In order to do this, `FeniXWizard` configures and installs required packages automatically, so you don't have to.

## Roadmap

* Babel support
* Auto configuration for XO and JSHint linters
* nwjs live reload support
* Live package install
* Frontend GUI