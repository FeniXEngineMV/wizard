---
layout: /layouts/documentation.html
---

# Conditional Compilation

FeniXWizard now supports conditional compilation, allowing you to create plugins compatible with both RPG Maker MV and MZ using the same codebase.

## How to Use Conditional Compilation
Conditional compilation is always enabled by default. To use it, wrap your code in special comments.

### For MV
To include code only in the MV version:

```js
// #if _MV
const varOnlyForMV = true
// #endif
```

This code block will only appear in the MV version of your plugin.

### For MZ
To include code only in the MZ version, you can either check for MZ directly or ensure it's not MV:

```js
// #if !_MV
const varOnlyForMZ = true
// #endif
```

Alternatively, you can use the MZ flag:

```js
// #if _MZ
const varOnlyForMZ = true
// #endif
```

### Create a Compatible Window Class
Here's an example of a window class compatible with both MV and MZ:

```js
export default class CustomWindow extends Window_Base {
  constructor (rect) {
    /* #if _MV
    super()
    super.initialize(rect.x, rect.y, rect.width, rect.height)
    // #elif _MZ */
    super(rect)
    // #endif
  }
}
```

This will output the appropriate code for each engine:

```js
// mz/js/plugins/Plugin.js

export default class CustomWindow extends Window_Base {
  constructor (rect) {
    super(rect)
  }
}
```

```js
// mv/js/plugins/Plugin.js

export default class CustomWindow extends Window_Base {
  constructor (rect) {
    super()
    super.initialize(rect.x, rect.y, rect.width, rect.height)
  }
}
```

## Auto Skip Builds if Missing flags (experimental)

`FeniXWizard` has an option to automatically skip a build if a flag is not included in your plugin's source code.

 For example, if you're building a plugin and you don't use the flag _FREE as part of the conditional compilation, then FeniXWizard will automatically skip any output with the `free` flag during the build phase.

By default, builds with `mz` and `pro` as their flags will always build, no matter what, unless you use your own `wizard.config.js` file to override the default build outputs.

{% message "Warning", "is-warning" %}
**Currently, there are issues with skipping builds and using multiple flags.** For example, if you are using the _`PRO` flag in your code, it's safe to assume you want a free version of your plugin. However, if you aren't using the `_FREE` flag in your code, the build will be skipped. In this situation, you will need to add an empty block with the `_FREE` flag somewhere in your code.

For example:
```js
// #if _FREE
// #endif
```
{% endmessage %}

## Custom Conditional Compilation Flags

FeniXWizard allows you to define custom flags for conditional compilation. By default, the following flags are active:

`_FREE` - By default this is active for both MV and MZ builds.

`_MZ` - This flag is only active during the MZ build phase

`_MZ` - This flag is only active during the MV build phase.

### Adding Custom Flags

You can add custom flags via the CLI or by using a wizard.config.js file.

#### Via the CLI

```
fenix build --flags customFlag,mz
```

This will build the plugin with _CUSTOMFLAG and _MZ as active flags.

#### Via the `wizard.config.js`


```js
export default {
  // wizard config options... 
  output: [
    {
      flags: ['customFlag', 'mz'],
      parameters: 'Parameters',
    },
    // other outputs...
  ]
}
```

When bundling, this output will include _CUSTOMFLAG and _MZ as active flags.

### A Configuration That Builds a Free and Pro Version of Your Plugin

```js
export default
{
  target: './src/',
  bundler: 'default',
  output: [
    {
      flags: ['free', 'mv'],
      skipIfMissingFlag: true,
      destination: 'games/',
      gameDir: 'mv',
      parameters: 'ParametersMV',
      affix: 'Free'
    },
    {
      flags: ['free', 'mz'],
      skipIfMissingFlag: true,
      destination: 'games/',
      gameDir: 'mz',
      parameters: 'Parameters',
      affix: 'Free'
    },
    {
      flags: ['pro', 'mv'],
      skipIfMissingFlag: true,
      destination: 'games/',
      gameDir: 'mv',
      parameters: 'ParametersMV'
    },
    {
      flags: ['pro', 'mz'],
      skipIfMissingFlag: false,
      destination: 'games/',
      gameDir: 'mz',
      parameters: 'Parameters'
    }
  ]
}

```