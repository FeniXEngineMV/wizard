---
layout: /layouts/documentation.html
---

# Workflow & Environment
`FeniXWizard` ensures you have a clean and consistent work environment. It does this by generating a project structure and giving you access to an easy to use workflow.

## Project Structure

`FeniXWizard` works with two different project structures, one for developing a single plugin and another for developing multiple plugins. `FeniXWizard` does not require configuration, no matter what style you choose. It automatically searches the `./src` directory and all of it's sub directories for a `main.js` and `parameters.js` and builds your plugins based on those files.

### Single Plugin Structure
A single plugin structure is good for prototyping and for plugins that aren't a part of a plugin suite.

```
├── game
├── src
│  ├── main.js
│  ├── Parameters.js
├── test
├── LICENSE
├── README.md
└── package.json
```

### Multiple Plugin Structure
A multiple plugin structure is good for creating a modular plugin suite or library that may interact with each other.
```
├── game
├── src
│  ├── plugin
│  │   ├── main.js
│  │   ├── Parameters.js
│  ├── plugin-two
│  ├── plugin-three
├── test
├── LICENSE
├── README.md
└── package.json
```

## Multiple File Plugins

Having one plugin file named `MyPlugin.js` compared to having it split into smaller files like `MyWindow.js`, `Parameters.js`, `Game_System.js`, allows you to quickly find what you're looking for. It also follows a Modular/D.R.Y approach, and if you code your plugin in sections that operate independently of each other, then this offers you the ability to use the same code in other plugin projects.

![plugin-directory](/img/multi-file-plugin.png)

## The Entry Point

I'm sure you're wondering how it is that `FeniXWizard` knows how to correctly bundle the plugin from multiple files. Well, that's what the `main.js` file is for, it's an entry point, from this file you will be responsible for importing your other modules. The modules which are imported are then processed by `FeniXWizard` and it will bundle the modules you've referenced.

A basic `main.js` file will look something like this:

```js
import './Scenes/Scene_Map.js'
// import 'otherModule/
```

You will not be required to import everything into this one file, in fact it's better to only import what is needed for a specific module.

For example, we have a new file named `Window_MyWindow.js` and I have a Scene in which I would like to use this window on `Scene_Map`. Instead of importing all those into the `main.js` entry file. We would instead export `Window_MyWindow` and import it in `Scene_Map` like so:


Export `Window_MyWindow` so we can import it elsewhere:
```js
// Windows/Window_MyNewWindow.js
export class Window_MyWindow extends Window_Base {
    constructor () {
        super()
    }
}

```
Import `Window_MyWindow` so we can create it in `Scene_Map`
```js
// Scenes/Scene_Map.js
import {Window_MyWindow} from './Windows/Window_MyWindow'

const createAllWindows = Scene_Map.prototype.createAllWindows

Scene_Map.prototype.createAllWindows = function () {
    createAllWindows.call(this)
    this._myWindow = new Window_MyWindow()
}

```
Then to make sure it all comes together into a pretty bundle , we simply import `Scene_Map.js` into the `main.js` file as seen in the example for `main.js` above.

## Exporting to Global

Exporting modules can be pretty important, in fact, if you have an entire API that you need to expose to the user of the plugin, then you will need to export important members from your modules.

To do this, all you have to do is export from the `main.js` file:

```js
export './Windows/Window_MyWindow.js'
// import 'otherModule/
```

Doing this will expose `Window_MyWindow` to the final bundle's namespace, which can be [configured](./parameters.html#modulename) in your plugins parameters file.
