---
layout: /layouts/documentation.html
---

# Parameters

Plugin parameters are the core file that RPG Maker uses to allow game developers to interact with the plugin. Without a parameters file, `FeniXWizard` cannot correctly build your plugin.

`FeniXWizard` automatically includes `Parameters.js` at the top of your final plugin bundle, so you don't need to manually include it in your `main.js` file.

## New Tags
`FeniXWizard` utilizes new parameter tags. While a few are optional, others are not and will produce warnings or errors.

```js
/*:
 * @plugindesc My plugin description
 * @author FeniXEngine Contributors
 *
 * New Tags -
 *
 * @pluginname MyPlugin
 * @modulename $myPluginGlobal
 * @external external-library
*/
```
## pluginname <Badge text="required"/>

 `@pluginname` This is the filename of the final bundle, which will be created to the default `'game/js/plugins/` directory.

## modulename <Badge text="optional"/>

`@modulename` is used when you are creating a plugin that will be used by other plugin developers or you want to create a global namespace and expose important members. The module name will be the namespace created when bundling.

For example a modulename as `$myPluginGlobal` will result in a final bundle:

```js
var $myPluginGlobal = (function (exports) {
    // ModuleA...

    //ModuleB...

    //Exports
    exports.myPluginString = 'MyPlugin'
}
```

{% message "Note", "is-warning" %}
If exporting members to the global namespace, then not having this tag will produce a warning
{% endmessage %}

## external <Badge text="optional"/>

`@external` tells `FeniXWizard` about the external libraries your plugin uses. This helps `FeniXWizard` create a smaller plugin file by excluding these libraries. You can use any libraries or frameworks you like in your plugin.

Important: If you're using a library that's not part of RPG Maker, you need to tell FeniXWizard about it using `@external`. **You'll also need to include this library as a separate file with your plugin**. This ensures the plugin works correctly when distributed.

Default included packages: 'fs', 'http', 'https', 'path', 'PIXI'.