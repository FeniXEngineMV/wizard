module.exports = {
  siteName: 'FeniXWizard',
  title: 'FeniXWizard - A Wizard for RPG Maker Plugins',
  heroImage: '/img/logos/hero-image.png',
  slogan: 'A lightweight, easy to use RPG Maker plugin development tool',
  buttonLink: '/guide/',
  navbarItems: [
    { text: 'Guide', href: '/guide' },
    { text: 'CLI Options', href: '/cli-options' }
  ],
  customToc: [{
    mainPage: '/guide',
    links: [
      { text: 'Guide', href: '/guide' },
      { text: 'Getting Started', href: '/guide/getting-started' },
      { text: 'Workflow & Environment', href: '/guide/workflow-environment' },
      { text: 'Parameters', href: '/guide/parameters' },
      { text: 'Bundler Plugin System', href: '/guide/bundler-plugin-system' },
      { text: 'Conditional Compilation', href: '/guide/conditional-compilation' }
    ]
  }]
}
