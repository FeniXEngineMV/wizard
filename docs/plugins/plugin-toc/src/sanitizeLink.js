function sanitizeLink (href) {
  const isExternalLink = /^https?:\/\//.test(href) || /\.\w+$/.test(href)

  if (isExternalLink) {
    return href
  } else {
    if (!href.endsWith('/')) {
      return `${href}/`
    }
    return href
  }
}

module.exports = sanitizeLink
