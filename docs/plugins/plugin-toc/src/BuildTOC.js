const cheerio = require('cheerio')

const NestHeadings = require('./NestHeadings')
const BuildList = require('./BuildList')
const sanitizeLink = require('./sanitizeLink')

const defaults = {
  tags: ['h2', 'h3', 'h4'],
  wrapper: 'nav',
  wrapperClass: 'toc',
  wrapperLabel: undefined,
  ul: false,
  flat: false
}

const BuildTOC = (text, page, userOptions) => {
  const options = { ...defaults, ...userOptions }
  const { tags, wrapper, wrapperClass, wrapperLabel, ul, flat } = options

  const $ = cheerio.load(text)

  let headings = []
  const pageHeadings = NestHeadings(tags, $)

  if (options.customToc) {
    let order = 0
    const pageToc = options.customToc.find(toc => page.inputPath.includes(toc.mainPage))

    if (pageToc) {
      for (let i = 0; i < pageToc.links.length; i++) {
        const heading = pageToc.links[i]

        if (sanitizeLink(heading.href) === page.url) {
          heading.children = pageHeadings
        }

        const mainHeading = pageHeadings.find(h => h.text === heading.text)
        if (mainHeading || sanitizeLink(heading.href) === page.url) {
          pageHeadings[0].text = heading.text
          pageHeadings.forEach(h => {
            h.order = h.order + order
            order++
          })
          continue
        }

        headings.push({
          id: heading.href,
          tag: 'h1',
          parent: true,
          order: i,
          text: heading.text,
          children: [],
          isLink: true
        })

        order++
      }
    }
  }

  headings = headings.concat(pageHeadings)

  const label = wrapperLabel ? `aria-label="${wrapperLabel}"` : ''

  return wrapper
    ? `<${wrapper} class="${wrapperClass}" ${label}>
        ${BuildList(headings, ul, flat)}
      </${wrapper}>`
    : BuildList(headings, ul, flat)
}

module.exports = BuildTOC
