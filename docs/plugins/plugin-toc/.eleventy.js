const buildTOC = require('./src/BuildTOC')
const parseOptions = require('./src/ParseOptions')

module.exports = (eleventyConfig, userOptions = {}) => {
  eleventyConfig.addShortcode('toc', (content, page) => {
    return buildTOC(content, page, userOptions)
  })
}
