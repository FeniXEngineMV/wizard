import resolve from '@rollup/plugin-node-resolve'
import eslint from 'rollup-plugin-eslint-bundle'

export default {
  input: `./src/main.js`,
  external: [
    'fs-extra',
    'http',
    'https',
    'path',
    'tracer',
    'cross-spawn',
    'chokidar',
    'commander',
    'ws',
    'pretty-error',
    'ora',
    'prompts',
    'nanospinner',
    'inquirer',
    'rollup',
    'browser-sync',
    '@rollup/plugin-typescript',
    'rollup-plugin-eslint-bundle',
    '@rollup/plugin-node-resolve',
    '@rollup/plugin-commonjs',
    '@fenixengine/rollup-plugin-jscc',
    'chalk',
    'adm-zip'
  ],
  output: [{
    file: `./bin/fenix-wizard.cjs`,
    format: 'cjs',
    indent: false,
    banner: '#!/usr/bin/env node',
    sourcemap: process.env.NODE_ENV === 'test',
    exports: 'auto'
  }, {
    file: `./bin/fenix-wizard.js`,
    format: 'esm',
    indent: false,
    banner: '#!/usr/bin/env node',
    sourcemap: process.env.NODE_ENV === 'test',
    exports: 'auto'
  }
  ],
  plugins: [
    resolve({
      mainFields: ['module', 'main']
    }),
    eslint({
      useEslintrc: true,
      fix: true
    })
  ]
}
