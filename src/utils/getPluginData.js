import fs from 'fs/promises'
import { fileExists } from './fileExists.js'
import { getPluginTag } from './getPluginTag.js'

export async function getPluginData (parametersPath, onWarn) {
  let plugin = null
  const paramsExist = await fileExists(parametersPath)
  if (!paramsExist) {
    return plugin
  }
  const parameters = await fs.readFile(parametersPath, 'utf8')

  try {
    plugin = {
      name: await getPluginTag(parameters, 'pluginname', onWarn) || 'FeniXPlugin',
      modulename: await getPluginTag(parameters, 'modulename', onWarn),
      external: await getPluginTag(parameters, 'external', onWarn) || [],
      parameters
    }
  } catch (error) {
    throw error
  }

  return plugin
}
