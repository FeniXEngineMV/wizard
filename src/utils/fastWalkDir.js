import fs from 'fs-extra'
import path from 'path'

export function fastWalkDir (dir, exclusions = [], result) {
  const files = fs.readdirSync(dir)

  for (let i = 0; i < files.length; i++) {
    const file = path.join(dir, files[i])

    if (fs.statSync(file).isDirectory()) {
      if (!exclusions.includes(path.basename(file))) {
        fastWalkDir(file, exclusions, result)
      }
    } else {
      result.push(file)
    }
  }
}
