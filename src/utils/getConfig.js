import { pathToFileURL } from 'url'
import { resolve } from 'path'
import defaultConfig from '../defaultConfig.js'
import { mergeStrings } from './mergeStringsWithUnderscore.js'

export async function getConfig (options = {}) {
  let configData = null
  let configPath = null

  if (typeof options === 'string') {
    configPath = resolve(options)
  } else if (options.config) {
    configPath = resolve(options.config)
  } else {
    configPath = undefined
  }

  const loadConfig = async (filePath) => {
    const module = await import(pathToFileURL(filePath))
    if (module.default) {
      return module.default
    }
  }

  if (configPath) {
    const config = await loadConfig(`${configPath}/wizard.config.js`)
    configData = { ...options, ...config }
  } else {
    configData = { ...defaultConfig, ...options }
  }

  const configFlags = configData.output.map((config) => config.flags).flat()
  const flagsToMerge = options.flags
    ? options.flags.filter(flag => !configFlags.includes(flag))
    : []

  const mergedOutputs = configData.output.map((output) => {
    const skipIfMissingFlag = options.skipIfMissingFlag ?? output.skipIfMissingFlag ?? false
    const destination = options.destination ?? output.destination ?? './game/'
    const affix = mergeStrings(null, output.affix, options.affix)

    return {
      ...output,
      affix,
      skipIfMissingFlag,
      destination,
      flags: flagsToMerge ? [...output.flags, ...flagsToMerge] : output.flags || []
    }
  })

  const resolvedOutputs = mergedOutputs.map((output) => ({
    ...output,
    destination: resolve(output.destination)
  }))

  configData.target = resolve(configData.target)
  configData.output = resolvedOutputs
  configData.configFilepath = resolve(`${configPath}/wizard.config.js`)

  return configData
}
