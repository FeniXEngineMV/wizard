import { spawn as crossSpawn } from 'cross-spawn'

export async function pipeSpawn (command, params, options = {}) {
  return new Promise((resolve, reject) => {
    const cp = crossSpawn(command, params, {
      env: process.env,
      shell: true,
      ...options
    })

    let stdoutData = ''
    let stderrData = ''

    cp.stdout.setEncoding('utf8').on('data', (data) => {
      stdoutData += data
    })

    cp.stderr.setEncoding('utf8').on('data', async (data) => {
      stderrData += data
    })

    cp.on('error', (err) => {
      reject(err)
    })

    cp.on('close', (code) => {
      if (code === 0) {
        resolve(stdoutData)
        return
      }
      reject(stderrData)
    })
  })
}
