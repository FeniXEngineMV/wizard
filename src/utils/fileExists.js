import fs from 'fs/promises'

export async function fileExists (filePath) {
  try {
    await fs.stat(filePath)
    return true
  } catch (err) {
    if (err.code === 'ENOENT') {
      return false
    } else {
      console.error('Error checking file existence:', err.message)
    }
  }
}
