import fs from 'fs-extra'

const toKebabCase = str => {
  return str && str
    .match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
    .map(x => x.toLowerCase())
    .join('-')
}

export async function createPackageJson (options) {
  const packagePath = `${options.destination}/package.json`
  try {
    const packageJsonExists = await fs.exists(packagePath)
    if (packageJsonExists) {
      return
    }
    await fs.writeJson(packagePath, {
      name: toKebabCase(options.name),
      description: options.description,
      author: options.author,
      version: '0.1.0',
      license: 'MIT',
      repository: { private: true },
      devDependencies: {},
      dependencies: {}
    }, { spaces: 4 })
  } catch (error) {
    throw new Error(error)
  }
}
