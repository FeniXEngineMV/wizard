import https from 'https'
import url from 'url'

export async function downloadFile (config = {}) {
  return new Promise((resolve, reject) => {
    if (!config.url) {
      reject(new Error('A url is required to download a file'))
    }
    const downloadUrl = url.parse(config.url)
    const httpsConfig = {
      hostname: downloadUrl.hostname,
      port: config.port || 443,
      path: downloadUrl.href,
      protocol: downloadUrl.protocol,
      agent: config.agent || https.globalAgent
    }

    https.get(httpsConfig,
      (response) => {
        const chunks = []
        const status = response.statusCode

        if (status !== 200) {
          reject(new Error(`Failed to load, response status code is ${status}`))
        }

        response.on('data', (chunk) => chunks.push(chunk))
        response.on('end', () => resolve(Buffer.concat(chunks)))
        response.on('error', (err) => reject(err))
      })
  })
}
