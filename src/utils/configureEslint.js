import fs from 'fs-extra'

function _packageToExtend (plugins) {
  return plugins.standard ? 'standard' : plugins.airbnb
    ? 'airbnb' : plugins.semistandard ? 'semistandard' : 'eslint:recommended'
}

function _isRpgmakerPlugin (plugins) {
  return typeof plugins['eslint-plugin-rpgmaker'] !== 'undefined'
}

export async function configureEslint (destination, plugins) {
  const packagePath = `${destination}/package.json`

  const pluginsObj = plugins.reduce((acc, cur) => {
    acc[cur.replace("'", '')] = true
    return acc
  }, {})

  try {
    const packageData = await fs.readJson(packagePath)
    const isRpg = _isRpgmakerPlugin(pluginsObj)
    const extender = _packageToExtend(pluginsObj)

    await fs.writeJson(packagePath, Object.assign(packageData, {
      eslintConfig: {
        extends: extender,
        plugins: [isRpg ? 'rpgmaker' : ''],
        parserOptions: {
          ecmaVersion: 'latest',
          sourceType: 'module'
        },
        env: {
          'rpgmaker/mv': isRpg,
          'rpgmaker/mz': isRpg
        },
        globals: {},
        rules: {
          'camelcase': 'off'
        }
      },
      eslintIgnore: [
        '/games/'
      ]
    }), { spaces: '\t', EOL: '\n' })
  } catch (error) {
    throw new Error(error)
  }
}
