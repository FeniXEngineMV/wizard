export function filterText (text, regex, action) {
  const result = []
  let match
  const re = regex
  while (match = re.exec(text)) { // eslint-disable-line no-cond-assign
    if (action(match)) {
      result.push(match)
    }
  }
  return result
}
