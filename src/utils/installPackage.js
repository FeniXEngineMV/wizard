import { logger } from './logger.js'
import { pipeSpawn } from './pipeSpawn.js'

async function installPeerDeps (module, path) {
  try {
    const result = await pipeSpawn('npm', ['info', module, 'peerDependencies', '--json'])
    const peerDeps = JSON.parse(result)

    for (const dependency in peerDeps) {
      const module = `${dependency}@${peerDeps[dependency]}`
      await pipeSpawn('npm', ['install', module, '--save-dev'], {
        cwd: path
      })
    }
  } catch (error) {
    throw new Error(logger.error(error))
  }
}

export async function install (modules, options = {}) {
  const { saveDev = true, path = process.cwd(), peers = false } = options
  const command = 'npm'
  const subCommand = 'install'
  const args = []

  if (options.peers) {
    logger.spinner.start({
      text: `Installing peer dependencies for package(s) ${modules}`
    })
    await installPeerDeps(modules[0], path)
    logger.spinner.success({
      text: `Success installing peer dependencies for package(s) ${modules}`
    })
  }

  args.push(subCommand, ...modules)

  if (saveDev) {
    args.push(peers ? '-dev' : '-D')
  }

  logger.spinner.start({
    text: `Installing package(s) ${modules}`
  })
  try {
    await pipeSpawn(command, args, {
      cwd: path,
      ignoreWarnings: true,
      ignoreInfo: true
    })
    logger.spinner.success({
      text: `Success installing package(s) ${modules}`
    })
  } catch (error) {
    logger.spinner.error({
      text: `Failed installing package(s) ${modules}`
    })
    Promise.reject(logger.error(`Failed to install modules ${modules}`))
  }
}
