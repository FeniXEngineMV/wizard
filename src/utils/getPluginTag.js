import { filterText } from './filterText.js'
import { logger } from './logger.js'

/**
 * Retrieves a tag of choice from a plugins parameters by looking for a
 * match in the plugins Parameters.js file.
 *
 * Only works for single line tags like author, plugindesc and custom tags for
 * FeniXEngine.
 *
 * @export
 * @param {string} parameters - The raw string from the Parameters.js file.
 * @param {string} tag - The tag to extract from the plugins parameters.
 * @returns {string} The value of the tag
 */
export async function getPluginTag (parameters, tag, onWarn) {
  try {
    const pattern = new RegExp(`@(${tag})([^\\r\\n]*)`, 'g')
    const match = filterText(parameters, pattern, match => match[1] === tag)[0]

    if (match) {
      return match[2].trim()
    } else {
      if (onWarn) {
        onWarn(`Tag @${tag} not found in Parameters.js`)
      }
    }
  } catch (error) {
    throw new Error(logger.error(error))
  }
}
