export function timestamp () {
  const options = { hour: '2-digit', minute: '2-digit', second: '2-digit' }
  const formatter = new Intl.DateTimeFormat('en-US', options)
  const formattedTime = formatter.format(new Date())
  return `[${formattedTime}]`
}
