import { parseShortPath } from './parseShortPath.js'
import config from '../config.js'

import nanoSpinner from 'nanospinner'
import PrettyError from 'pretty-error'

import readline from 'readline'
import { timestamp } from './timeStamp.js'

export function Logger (options = {}) {
  const _options = Object.assign(options, {
    silent: false,
    logLevel: 4,
    timestamp: true,
    useSpinner: true
  })
  let _currentLine = 0
  const _savedLines = []
  const _currentTime = () => timestamp()
  const prettyError = new PrettyError()

  const getChalk = async () => {
    const chalk = (await import('chalk')).default
    return chalk
  }

  const _spinner = nanoSpinner.createSpinner()

  const _timeLog = async () => {
    const chalk = await getChalk()
    return chalk.blue(_currentTime())
  }

  const _pathLog = async (filename, path) => {
    const chalk = await getChalk()
    return chalk.green(`${filename} -> ${parseShortPath(path)}`)
  }

  const _serverBadgeLog = async (target, port) => {
    const chalk = await getChalk()
    return `
  ===============================
  ${chalk.blue.bold('FeniXWizard')} v${config.version}\n
  Serving => ${parseShortPath(target)}\n
  ${chalk.bold('Game')} => ${chalk.blue.bold(`http://localhost:${port}`)}
  ===============================
  `
  }
  const _buildMessage = async (message, filename, path) => {
    const time = await _timeLog()
    const pathLog = await _pathLog(filename, path)
    return `${time} ${message} ${pathLog}`
  }

  function _saveLine () {
    _savedLines.push(_currentLine)
    return _currentLine
  }

  function _clearSavedLines () {
    _savedLines.forEach(line => {
      _clearLine(line)
      _savedLines.splice(_savedLines.indexOf(line), 1)
    })
  }

  function _clearLine (line) {
    if (line) {
      readline.moveCursor(process.stdout, 0, -1, () => {
        readline.clearScreenDown(process.stdout, () => {
          _currentLine--
        })
      })
    }
  }

  function _write (message) {
    if (_options.silent) {
      return message
    }
    _clearSavedLines()
    _currentLine++
    process.stdout.write(`${message}\n`)
    return message
  }

  return {
    get spinner () {
      if (!_options.useSpinner) {
        return {
          start: (options) => {
            this.log(options.text)
          },
          success: (options) => {
            this.log(options.text)
          },
          error: (options) => {
            this.log(options.text)
          }
        }
      }
      return _spinner
    },

    saveLog (message) {
      this.log(message)
      _saveLine()
    },

    setOptions (options) {
      Object.assign(_options, options)
    },

    hideCursor () {
      // cliCursor.hide()
    },

    clear () {
      console.clear()
    },

    async log (message) {
      if (_options.logLevel > 3) {
        const chalk = await getChalk()
        return _write(chalk.bold(message))
      }
    },

    async logServerBadge (target, port) {
      _write(await _serverBadgeLog(target, port))
    },

    async logBuild (filename, path, type = 'succeed') {
      const failedMessage = await _buildMessage('Failed', filename, path)
      const succeedMessage = await _buildMessage('Bundled', filename, path)

      switch (type) {
        case 'succeed':
          return _write(succeedMessage)
        case 'failed':
          _write(failedMessage)
      }
    },

    async warn (message, badge = true) {
      const chalk = await getChalk()
      const badgeMessage = badge ? 'Warning' : ''
      const styledMessage = `${chalk.black.bgYellow(badgeMessage)} ${chalk.yellow(message)}`
      return _write(styledMessage)
    },

    error (message) {
      if (_options.logLevel > 1) {
        return _write(prettyError.render(new Error(message)))
      }
    }
  }
}

export const logger = Logger()
