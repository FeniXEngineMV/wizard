import { logger } from './logger.js'
import { pipeSpawn } from './pipeSpawn.js'
import fs from 'fs-extra'

export async function initializeGit (options = {}) {
  try {
    const { path = process.cwd() } = options
    const args = []
    const command = 'git'
    const subCommand = 'init'
    const quietCommand = '--quiet'
    args.push(subCommand, quietCommand)

    await pipeSpawn(command, args, {
      cwd: path
    })

    await fs.writeFile(`${path}/.gitignore`, 'games\nnode_modules')

    logger.spinner.success({
      text: `Success initializing git`
    })
  } catch (error) {
    logger.spinner.error({
      text: `Failed initializing git`
    })
  }
}
