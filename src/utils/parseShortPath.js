import path from 'path'

export function parseShortPath (fullPath) {
  const cwd = process.cwd()
  fullPath = path.resolve(fullPath)

  if (fullPath.startsWith(cwd)) {
    return path.relative(cwd, fullPath)
  } else {
    return fullPath
  }
}
