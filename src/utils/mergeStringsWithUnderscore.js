/**
 * Merges strings together connecting them together with a separator
 *
 * @param {Object} options
 * @param {string} ...strings
 * @returns {string}
 */
export function mergeStrings (options, ...strings) {
  const defaultOptions = {
    separator: '_',
    allowEmpty: false,
    defaultValue: '',
    trim: true
  }

  const mergedOptions = { ...defaultOptions, ...options }

  const result = strings
    .filter(str => {
      if (typeof str !== 'string') {
        return false
      }
      if (mergedOptions.allowEmpty) return true
      return str !== ''
    })
    .map(str => mergedOptions.trim ? str.trim() : str)
    .join(mergedOptions.separator)

  return result || mergedOptions.defaultValue
}
