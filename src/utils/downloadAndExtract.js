import AdmZip from 'adm-zip'
import fs from 'fs-extra'
import { logger } from './logger.js'
import { downloadFile } from './downloadFile.js'

export async function downloadAndExtract (url, destination, filename, renamedDir) {
  await fs.ensureDir(destination)
  logger.spinner.start({
    text: 'Downloading FeniX Lightweight MV demo'
  })
  const zipDirName = filename.replace('.zip', '')
  const demoData = await downloadFile({ url })
  // Extract downloaded zip
  const zip = new AdmZip(demoData)
  zip.extractAllTo(destination, true)

  if (renamedDir) {
    await fs.rename(`${destination}/${zipDirName}`, `${destination}/${renamedDir}`)
  }
  logger.spinner.success({
    text: `${filename} Download complete`
  })
}
