import { Transform } from 'stream'

export default class extends Transform {
  constructor (options) {
    super(options)
    this._options = options
  }

  _transform (chunk, encoding, onComplete) {
    if (this._options.onTransform) {
      this._options.onTransform(chunk, this)
      onComplete()
    }
  }
}
