import fs from 'fs-extra'

export async function createTsconfig (options) {
  const tsconfigPath = `${options.destination}/tsconfig.json`
  try {
    const tsconfigExists = await fs.exists(tsconfigPath)
    if (tsconfigExists) {
      return
    }
    await fs.writeJson(tsconfigPath, {
      compilerOptions: {
        module: 'es6',
        moduleResolution: 'node',
        target: 'es6'
      },
      include: ['node_modules/@fenixengine/rmmz-ts/libs/**/*', 'src/**/*']
    }, { spaces: 4 })
  } catch (error) {
    throw new Error(error)
  }
}
