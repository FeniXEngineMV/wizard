import fs from 'fs-extra'
import _path from 'path'
import { logger } from './logger.js'

export const hasMain = async function (path) {
  try {
    const files = await fs.readdir(path)
    if (!files) {
      return false
    }
    return files.some(file => _path.parse(file).name === 'main')
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}
