import { logger } from './utils/index.js'
import * as chokidar from 'chokidar'

let watcher = null
let fileChangeCount = 0
const listeners = []

export function createFileWatcher (pathOrPaths, listener) {
  if (watcher) {
    watcher.close()
  }

  if (typeof listener === 'function') {
    listeners.push(listener)
  }

  const paths = Array.isArray(pathOrPaths) ? pathOrPaths : [pathOrPaths]

  watcher = chokidar.watch(paths, {
    persistent: true,
    ignoreInitial: true,
    awaitWriteFinish: true,
    usePolling: true,
    ignored: [
      /node_modules/,
      /dist/,
      '**/*.log'
    ]
  })

  watcher.on('ready', () => {
    setTimeout(() => {
      logger.log('👁️ Watching for changes...')
    }, 1500)
  })

  watcher.on('change', async (path, stats) => {
    fileChangeCount++
    if (fileChangeCount > 1) {
      return
    }
    if (listeners.length > 0) {
      logger.clear()
      for (const listener of listeners) {
        await listener(path, stats)
        logger.log('👁️ Watching for changes...')
      }
      fileChangeCount = 0
    }
  })

  watcher.on('error', (error) => {
    logger.error(error)
  })

  return {
    addListener: (listener) => {
      if (typeof listener === 'function') {
        listeners.push(listener)
      }
    },
    removeListener: (listener) => {
      if (typeof listener === 'function') {
        listeners.splice(listeners.indexOf(listener), 1)
      }
    }
  }
}

export function watch (path, listener) {
  const watcher = createFileWatcher(path, listener)
  return watcher
}
