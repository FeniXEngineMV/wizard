export default
{
  target: './src/',
  bundler: 'default',
  output: [
    {
      flags: ['free', 'mv'],
      skipIfMissingFlag: true,
      destination: 'games/',
      gameDir: 'mv',
      parameters: 'ParametersMV',
      affix: 'Free'
    },
    {
      flags: ['free', 'mz'],
      skipIfMissingFlag: true,
      destination: 'games/',
      gameDir: 'mz',
      parameters: 'Parameters',
      affix: 'Free'
    },
    {
      flags: ['pro', 'mv'],
      skipIfMissingFlag: true,
      destination: 'games/',
      gameDir: 'mv',
      parameters: 'ParametersMV'
    },
    {
      flags: ['pro', 'mz'],
      skipIfMissingFlag: false,
      destination: 'games/',
      gameDir: 'mz',
      parameters: 'Parameters'
    }
  ]
}
