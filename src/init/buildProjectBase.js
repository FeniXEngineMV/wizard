import fs from 'fs-extra'
import { downloadAndExtract,
  createPackageJson,
  logger,
  install,
  configureEslint,
  initializeGit,
  createTsconfig
} from '../utils/index.js'

import config from '../config.js'

const CURR_DIR = process.cwd()
const MV_GAME_FILENAME = 'fenix-lightweight-mv-game-v0.0.1.zip'
const MV_GAME_LINK = `https://gitlab.com/FeniXEngineMV/fenix-lightweight-mv-game/-/archive/v0.0.1/${MV_GAME_FILENAME}`
const MZ_GAME_FILENAME = 'fenix-lightweight-mz-game-v0.0.1.zip'
const MZ_GAME_LINK = `https://gitlab.com/FeniXEngineMV/fenix-lightweight-mz-game/-/archive/v0.0.1/${MZ_GAME_FILENAME}`

export async function copyTemplateFiles (options) {
  const { destination, language } = options
  const templateDir = `${config.rootDir}/templates/base/`

  try {
    const files = await fs.readdir(templateDir)
    await fs.ensureDir(`${destination}/src/`)

    for (const file of files) {
      if (language === 'ts' && file === 'Core.js') {
        continue
      }
      const fileDir = `${templateDir}/${file}`
      const fileDestDir = `${destination}/src/${file}`
      const fileExists = await fs.exists(fileDestDir)

      if (fileExists === false) {
        await fs.copyFile(fileDir, fileDestDir)
        if (language === 'ts' && file !== 'Core.ts') {
          await fs.rename(`${destination}/src/${file}`, `${destination}/src/${file.replace('.js', '.ts')}`)
        }
      }
    }
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}

async function copyVsCodeLaunchJson (options) {
  const { destination } = options
  try {
    await fs.copyFile(`${config.rootDir}/templates/base/vscode/launch.json`, `${destination}/.vscode/launch.json`)
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}

async function injectParamterTags (options) {
  try {
    const { author, pluginname, destination, description, language } = options
    const filename = language === 'ts' ? 'Parameters.ts' : 'Parameters.js'
    const parameterData = await fs.readFile(`${destination}/src/${filename}`, 'utf8')
    const authorTag = `@author ${author}`
    const pluginnameTag = `@pluginname ${pluginname}`
    const descriptionTag = `@plugindesc ${description}`

    const newParameters = parameterData
      .replace(`@author`, `${authorTag}`)
      .replace(`@pluginname`, `${pluginnameTag}`)
      .replace(`@plugindesc`, `${descriptionTag}`)

    await fs.writeFile(`${destination}/src/${filename}`, newParameters)
  } catch (error) {
    throw new Error(logger.error(error))
  }
}

async function installLinterPackages (options) {
  if (options.linter === 'none' || !options.eslintPlugins) {
    return
  }
  const plugins = options.eslintPlugins
  const destination = options.destination
  try {
    await install([options.linter], { path: destination })

    for (const plugin of plugins) {
      if (plugin === 'standard') {
        await install(['eslint-config-standard'], { peers: true, path: destination })
      } else if (plugin === 'semistandard') {
        await install(['eslint-config-semistandard'], { peers: true, path: destination })
      } else if (plugin === 'airbnb') {
        await install(['airbnb'], { peers: true, path: destination })
      } else {
        await install([plugin], { path: destination })
      }
    }
    await configureEslint(destination, plugins)
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}

export async function downloadAndExtractMV (path) {
  await downloadAndExtract(MV_GAME_LINK, path, MV_GAME_FILENAME, 'mv')
}

export async function downloadAndExtractMZ (path) {
  await downloadAndExtract(MZ_GAME_LINK, path, MZ_GAME_FILENAME, 'mz')
}

export async function buildProjectBase (options) {
  try {
    if (fs.existsSync(options.destination)) {
      const stat = await fs.stat(options.destination)
      options.destination = stat.isDirectory() ? options.destination : `${CURR_DIR}/${options.destination}`
    } else {
      await fs.ensureDir(options.destination)
    }

    const destination = options.destination

    await createPackageJson({
      name: options.projectName,
      description: options.projectDescription,
      author: options.projectAuthor,
      destination
    })
    if (options.language === 'ts') {
      await createTsconfig({ destination })
    }
    await copyTemplateFiles(options)
    await injectParamterTags({
      author: options.projectAuthor,
      description: options.projectDescription,
      pluginname: options.projectName,
      language: options.language,
      destination
    })

    if (options.editor === 'vscode') {
      await copyVsCodeLaunchJson(options)
    }

    if (options.language === 'ts') {
      await install(['typescript'], { path: destination })
      await install(['@fenixengine/rmmz-ts'], { path: destination })
    }

    await installLinterPackages(options, destination)

    if (options.optionalPackages.length > 0) {
      await install(options.optionalPackages, { path: destination })
    }

    if (options.initializeGit) {
      initializeGit({ path: destination })
    }

    await fs.ensureDir(`${destination}/games`)
    if (options.downloadGames) {
      const gamesDestination = `${destination}/games/`
      for (const game of options.downloadGames) {
        if (game === 'mv') {
          await downloadAndExtractMV(gamesDestination)
        }
        if (game === 'mz') {
          await downloadAndExtractMZ(gamesDestination)
        }
      }
    }
    await fs.ensureDir(`${destination}/games`)
    await fs.ensureDir(`${destination}/games/mv`)
    await fs.ensureDir(`${destination}/games/mv`)
  } catch (error) {
    Promise.reject(logger.error(error))
  }
}
