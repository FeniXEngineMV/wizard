import { buildProjectBase } from './buildProjectBase.js'
import { logger } from '../utils/index.js'

import prompts from 'prompts'

export async function startInit (options) {
  const chalk = (await import('chalk')).default
  await logger.log(`
  ${chalk.blue('Welcome to FeniXWizard!')}
  FeniXWizard is a developer tool for creating an environment that makes developing
  RPG Maker MV plugins faster and easier.

  ${chalk.green('Follow the on-screen instructions to get started!.')}
  `)

  ;(async () => {
    const onCancel = (prompt, answers) => {
      answers.__cancelled__ = true
      return false
    }

    const questions = [
      {
        type: 'text',
        name: 'destination',
        message: 'Where would you like to create the project',
        initial: './'
      },
      {
        type: 'select',
        name: 'language',
        initial: 0,
        message: 'Do you want to create a plugin using TypeScript or JavaScript',
        choices: [
          {
            value: 'js',
            title: 'JavaScript'
          },
          {
            value: 'ts',
            title: 'TypeScript (experimental)'
          }
        ]
      },
      {
        type: 'text',
        name: 'projectName',
        message: 'Plugin name',
        initial: 'GeneratedPlugin'
      },
      {
        type: 'text',
        name: 'projectDescription',
        initial: '',
        message: 'Project description'
      },
      {
        type: 'text',
        name: 'projectAuthor',
        initial: 'FeniXEngine Contributers',
        message: 'Author name'
      },
      {
        type: process.env.TERM_PROGRAM === 'vscode' ? null : 'confirm',
        name: 'editor',
        initial: true,
        message: 'Are you using VSCode?'
      },
      {
        type: 'select',
        name: 'linter',
        initial: 0,
        message: 'Choose a linter',
        choices: [
          {
            value: 'eslint',
            name: 'ESLint'
          },
          {
            value: 'xo',
            name: 'XO'
          },
          {
            value: 'jshint',
            name: 'JSHint'
          },
          {
            value: 'none',
            name: 'none'
          }
        ]
      },
      {
        type: prev => prev === 'eslint' ? 'multiselect' : null,
        name: 'eslintPlugins',
        initial: [1, 2],
        message: 'Which ESLint plugins would you like to install',
        choices: [
          {
            value: 'eslint-plugin-rpgmaker',
            name: 'RPG Maker MV Global Variables'
          },
          {
            value: 'standard',
            name: 'StandardJS Config'
          },
          {
            value: 'semistandard',
            name: 'StandardJS Config w/ Semicolons'
          },
          {
            value: 'airbnb',
            name: 'Airbnb Style'
          }
        ]
      },
      {
        type: 'multiselect',
        name: 'optionalPackages',
        initial: [1],
        message: 'Would you like to install other useful packages',
        choices: [
          {
            value: '@fenixengine/tools',
            name: 'FeniXTools - A modular RPG Maker MV plugin library'
          },
          {
            value: 'ava',
            name: 'AVA - Futuristic test runner'
          },
          {
            value: 'flow-bin',
            name: 'Flow - A static type checker for JavaScript'
          }
        ]
      },
      {
        type: 'confirm',
        name: 'initializeGit',
        initial: true,
        message: 'Would you like to initialize git for this project?'
      },
      {
        type: 'multiselect',
        initial: [1, 2],
        name: 'downloadGames',
        message: 'Which lightweight demo games would you like to download',
        choices: [
          {
            value: 'mv',
            name: 'Download FeniX Lightweight MV Demo'
          },
          {
            value: 'mz',
            name: 'Download FeniX Lightweight MZ Demo'
          }
        ]
      }]

    const response = await prompts(questions, { onCancel })

    if (response.__cancelled__) {
      return
    }

    if (!response.vscode && process.env.TERM_PROGRAM === 'vscode') {
      response.vscode = true
    }

    buildProjectBase(response, onCancel)
  })()
}
