import { rollup } from 'rollup'
import typescript from '@rollup/plugin-typescript'
import commonjs from '@rollup/plugin-commonjs'
import nodeResolve from '@rollup/plugin-node-resolve'
import jscc from '@fenixengine/rollup-plugin-jscc'
import path from 'path'
import { logger } from './utils/index.js'

function convertToUpperCaseObject (array) {
  return array.reduce((acc, str) => {
    acc[`_${str.toUpperCase()}`] = true
    return acc
  }, {})
}

export function PluginRollup () {
  return {
    async bundle (options) {
      try {
        let shouldSkipBuild = false
        const input = path.resolve(options.target)
        const plugin = options.plugin
        const fileExtension = path.extname(input)
        const isTypeScript = fileExtension === '.ts'

        const external = ['fs', 'http', 'https', 'path', 'PIXI'].concat(plugin.external)
        const destination = `${options.destination}/${options.gameDir}`
        const filename = options.affix
          ? `${destination}/js/plugins/${plugin.name}_${options.affix}.js`
          : `${destination}/js/plugins/${plugin.name}.js`

        const generatePluginArray = (flags) => {
          const common = [
            nodeResolve({ module: true, preferBuiltins: false }),
            commonjs(),
            jscc({
              values: convertToUpperCaseObject(flags),
              asloader: false,
              onComplete (results) {
                const { foundExprs } = results

                if (foundExprs && options.skipIfMissingFlag) {
                  shouldSkipBuild = options.flags.some(flag => {
                    return !foundExprs.has(`_${flag.toUpperCase()}`)
                  })
                }
              }
            })
          ]

          if (isTypeScript) {
            return [typescript()].concat(common)
          }

          return common
        }

        const inputOptions = {
          input,
          external,
          plugins: generatePluginArray(options.flags),
          onwarn (warning) {
            if (warning.code === 'EVAL') {
              return
            }
            if (warning.code === 'UNRESOLVED_IMPORT') {
              if (process.env.CI) {
                throw new Error(warning.message)
              }
            }
            logger.warn(warning.message)
          }
        }

        const outputOptions = {
          file: filename.trim(),
          format: 'iife',
          name: plugin.modulename,
          footer: options.footer,
          banner: plugin.parameters,
          sourcemap: options.sourcemap,
          indent: false
        }

        const bundle = await rollup(inputOptions)
        await bundle.generate(outputOptions)

        if (shouldSkipBuild) {
          return { skipped: true, code: bundle, options }
        }

        await bundle.write(outputOptions)
        bundle.close()

        return { skipped: false, code: bundle }
      } catch (error) {
        if (error.frame) {
          logger.warn(`${error.name}: ${error.message}`, false)
          logger.log(error.frame)
        } else {
          logger.error(error)
        }
      }
    }
  }
}
