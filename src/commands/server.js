import { startServer } from './../server.js'

export function serverCommand (program) {
  program
    .command('serve')
    .description('Starts a server and syncs all changes')
    .argument('[source]', 'the source directory. defaults to ./src')
    .option('-d, --destination <path>', 'directory to serve from. defaults to ./games/')
    .option('-p, --port [number]', 'Choose which port to run the server on')
    .action(startServer)
}
