import { downloadAndExtract, logger } from '../utils/index.js'

const MV_GAME_FILENAME = 'fenix-lightweight-mv-game-v0.0.1.zip'
const MV_GAME_LINK = `https://gitlab.com/FeniXEngineMV/fenix-lightweight-mv-game/-/archive/v0.0.1/${MV_GAME_FILENAME}`
const MZ_GAME_FILENAME = 'fenix-lightweight-mz-game-v0.0.1.zip'
const MZ_GAME_LINK = `https://gitlab.com/FeniXEngineMV/fenix-lightweight-mz-game/-/archive/v0.0.1/${MZ_GAME_FILENAME}`

export function downloadCommand (program) {
  program
    .command('download')
    .description(`Downloads and extracts an RPG MAker demo game`)
    .argument('<engine>', 'engine to download, mv or mz')
    .option('-d, --destination <path>', 'the destination to extract the demo to')
    .action(async (engine, options) => {
      if (!options.destination) {
        logger.warn(`No destination specified, using default directory /games/`)
        options.destination = `./games/`
      }

      switch (engine) {
        case 'mv':
          await downloadAndExtract(MV_GAME_LINK, options.destination, MV_GAME_FILENAME, engine)
          break

        case 'mz':
          await downloadAndExtract(MZ_GAME_LINK, options.destination, MZ_GAME_FILENAME, engine)
          break
      }
    })
}
