import { logger } from '../utils/logger.js'
import { builder } from './../builder.js'

export function buildCommand (program) {
  program
    .command('build')
    .description(`Builds a plugin from it's source files`)
    .argument('[source]', 'the source directory. defaults to ./src')
    .argument('[desination]', 'the destination directory. defaults to ./games/[rpgmaker version]/js/plugins/')
    .option('-t, --target <path>', 'The target directory where your main.js and source code is.', './src')
    .option('-d, --destination <path>', 'The destination directory the final bundle will write to')
    .option('-c, --config [path]', 'Utilizes the config file at the root of the project or at <path>')
    .option('-a,--affix <affix>', 'Append an affix to the filename')
    .option('--tags <flags>', 'DEPRECATED: Enable conditional compilation for code under the flags', [])
    .option('-f,--flags <flags>', 'Enable conditional compilation for code under the flags', [])
    .option('-o, --only <dirs...>', 'Only build the specified project directory')
    .option('-w, --watch', 'Watches files to rebuild when changes occur')
    .option('-s, --sourcemap', 'choose to generate a sourcemap inline with the plugin')
    .option('-m, --skipMissingFlag', 'Skip a build if any flag is not included in the source code')
    .action((source, destination, options) => {
      if (options.config && typeof options.config === 'boolean') {
        options.config = '.'
      }
      if (options.skipMissingFlag) {
        options.skipIfMissingFlag = options.skipMissingFlag
      }

      if (options.destination) {
        destination = options.destination
      }

      if (options.target) {
        source = options.target
      }

      /**
      * DEPRECATED tags option. For now we replace flags with tags if tags is an
      * option.
      */
      if (options.tags.length > 0) {
        logger.warn('DEPRECATED: --tags option is deprecated in favor of --flags or -f')
        options.flags = options.tags
      }

      if (!Array.isArray(options.flags)) {
        options.flags.replace(',', ' ')
        options.flags = options.flags.split(' ')
        options.flags.map((flag, index) => {
          options.flags[index] = flag.trim()
        })
      }
      builder({
        target: source,
        destination,
        ...options
      })
    })
}
