import path from 'path'
import fs from 'fs-extra'
import { fileURLToPath } from 'url'

const isTest = process.env.NODE_ENV === 'test' || process.env.CI

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const rootDir = path.dirname(__dirname)

const rootPackage = JSON.parse(fs.readFileSync(`${rootDir}/package.json`))

const version = rootPackage.version

export default {
  rootDir,
  rootPackage,
  isTest,
  version
}
