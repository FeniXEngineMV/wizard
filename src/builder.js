
import fs from 'fs-extra'
import path from 'path'
import { getPluginData, getConfig, logger, fastWalkDir } from './utils/index.js'
import { PluginRollup } from './rollup.js'
import { watch } from './watcher.js'

let watcher = null
let config = null

async function buildFromDirectory (config) {
  const { exclusions = [] } = config
  const defaultExclusions = [
    'node_modules',
    'dist',
    'games',
    '.vscode',
    '.git',
    'docs',
    ...exclusions
  ]

  const files = []

  fastWalkDir(config.target, defaultExclusions, files)

  if (files.length === 0) {
    logger.warn(`No files found in ${config.target}.`)
  }

  const mainFilenames = ['main.js', 'main.ts']
  const mainFiles = files.filter(file => mainFilenames.includes(path.basename(file)))

  if (mainFiles.length === 0) {
    logger.warn(`No main files found in ${config.target}.`)
  }

  for (const file of mainFiles) {
    const fileDir = path.dirname(file)
    if (config.only && !config.only.some(dir => fileDir.includes(dir))) {
      continue
    }
    await build({ ...config, target: file })
  }
}

async function build (config) {
  try {
    const fileExt = path.extname(config.target)
    for (const output of config.output) {
      const plugin = await getPluginData(
        `${path.dirname(config.target)}/${output.parameters}${fileExt}`,
        onPluginDataWarn
      )
      if (!plugin) {
        await logger.warn(`Plugin parameters not found, skipping ${output.flags} bundle ${config.target}.`)
        continue
      }
      const bundleOptions = { ...config, ...output, plugin }
      const result = await config.bundler().bundle(bundleOptions)
      const name = output.affix ? plugin.name + output.affix : plugin.name
      const buildNumber = config.output.indexOf(output)

      if (result && result.skipped) {
        await logger.warn(`Missing flags ${output.flags}, skipping build #${buildNumber} for file ${name}`)
        continue
      }
      await logger.logBuild(name, output.destination)
    }
  } catch (error) {
    throw error
  }
}

export async function builder (options) {
  if (!config) {
    config = await getConfig(options)
    if (config.bundler === 'default') {
      config.bundler = PluginRollup
    }
  }

  if (!watcher && options.watch) {
    const target = options.only ? `${config.target}/${options.only}` : config.target
    const paths = options.config ? [config.configFilepath, target] : [target]
    watcher = watch(paths)
    watcher.addListener(async (path, stats) => {
      if (path === config.configFilepath) {
        logger.log('Config file changed. Rebuilding...')
      }
      await builder(options)
    })
  }

  try {
    const targetStat = fs.statSync(config.target)

    if (targetStat.isDirectory()) {
      await buildFromDirectory(config)
    } else if (targetStat.isFile()) {
      await build(config)
    }
  } catch (error) {
    throw error
  }
}

function onPluginDataWarn (message) {
  if (message.includes('@external')) {
    return
  }
  logger.warn(message)
}
