import { fileURLToPath } from 'url'
import {
  serverCommand,
  buildCommand,
  initCommand
} from './commands/index.js'

import config from './config.js'
import { buildProjectBase, downloadAndExtractMV, downloadAndExtractMZ } from './init/buildProjectBase.js'
import { builder } from './builder.js'
import { startInit } from './init/init.js'
import { program } from 'commander'
import { downloadCommand } from './commands/download.js'

function _loadCommands (program) {
  const commands = [serverCommand, buildCommand, initCommand, downloadCommand]
  commands.forEach(command => command(program))
}

_loadCommands(program)

const entryFile = process.argv?.[1]
const __filename = fileURLToPath(import.meta.url)

if (entryFile === __filename || process.argv.length > 1) {
  program
    .version(config.version)
    .action(() => {
      startInit()
    })
    .parse(process.argv)
}

// Expose to API
export default { builder, buildProjectBase, downloadAndExtractMV, downloadAndExtractMZ }
