import http from 'http'
import fs from 'fs-extra'
import path from 'path'

import { logger } from './utils/index.js'
import { createFileWatcher } from './watcher.js'
import { builder } from './builder.js'
import config from './config.js'
import Injector from './utils/Injector.js'
import { WebSocketServer } from 'ws'

export function startServer (source = './src', options) {
  const publicDir = path.normalize(options.destination || `${process.cwd()}/games/`)
  const port = options.port || 1818
  const socketPort = 9000
  const hostname = 'localhost'
  const clientReloadPath = `${config.rootDir}/src/reload-client.js`
  let fileStream = null

  const app = http.createServer((request, response) => {
    try {
      const fileUrl = path.normalize(`${publicDir}${request.url}`)

      switch (request.url) {
        case '/':
          const indexPath = `${publicDir}index.html`

          if (fs.existsSync(indexPath)) {
            fileStream = fs.createReadStream(indexPath)
            fileStream.pipe(new Injector({
              onTransform (chunk, stream) {
                const html = chunk.toString()
                const codeToInject = `<!-- Inserted by FeniXEngine MV Wizard -->
                <script type="text/javascript" src="/reload-client.js"></script>`
                const injectedHtml = html.replace(`</body>`, `${codeToInject}\n  </body>`)

                stream.push(injectedHtml)
              }
            })).pipe(response)
          } else {
            response.writeHead(404)
            response.end()
            throw new Error(logger.error('Unable to start server, cannot find index.html in target directory'))
          }
          break
        case '/reload-client.js':
          fileStream = fs.createReadStream(clientReloadPath)
          fileStream.pipe(new Injector({
            onTransform (chunk, stream) {
              const js = chunk.toString()
              const codeToReplace = `const socketServerUrl = ''`
              const codeToInject = `const socketServerUrl = 'ws://localhost:${socketPort}'`
              const toInject = js.replace(codeToReplace, codeToInject)

              stream.push(toInject)
            }
          })).pipe(response)
          break

        default:
          fileStream = fs.createReadStream(fileUrl)
          fileStream.pipe(response)
          break
      }

      fileStream.on('open', (data) => {
        response.writeHead(200)
      })

      fileStream.on('error', () => {
        response.writeHead(404)
        response.end()
      })
    } catch (error) {
      response.writeHead(500)
      response.end()
      socketServer.close()
      app.close()
      logger.error(error)
    }
  })

  // Setup socket server
  const socketServer = new WebSocketServer({ port: socketPort })
  let socketEvent = null

  // Start listening to server
  app.listen(port, hostname, async () => {
    await logger.logServerBadge(publicDir, port)
    const absoluteSourcePath = path.normalize(source)
    const watchDir = path.resolve(process.cwd(), absoluteSourcePath)
    createFileWatcher(watchDir, (path, stats) => {
      builder(options)
        .then(() => {
          if (socketEvent) {
            logger.saveLog('Reloading browser')
            socketEvent.send('reload')
          } else {
            logger.error('Unable to communicate with reload client')
          }
        })
        .catch(error => logger.error(error))
    })
  })

  socketServer.on('connection', ws => {
    ws.send(`FeniXWizard 🧙 - Successfully Connected! 🔌`)
    socketEvent = ws
  })

  socketServer.on('error', error => logger.error(error))

  const closeServers = function () {
    app.close()
    socketServer.close()
  }

  // Ensure we close all servers before FeniXWizard is terminated
  process.on('beforeExit', closeServers)
  process.on('unhandledException', closeServers)
  process.on('SIGINT', () => {
    closeServers()
    process.exit(0)
  })

  return {
    app,
    socketServer
  }
}
